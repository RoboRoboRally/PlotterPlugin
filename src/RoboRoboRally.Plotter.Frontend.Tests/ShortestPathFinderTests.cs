﻿using NUnit.Framework;
using RoboRoboRally.Plotter.Frontend.Game;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Plotter.Frontend.Tests
{
	[TestFixture]
    class ShortestPathFinderTests
	{
		[Test]
		public void PathFinding_PartiallyBlockedPath()
		{
			TestEnvironment environment = new TestEnvironment(2, new Model.MapDB.MapSize(10, 10));
			ITile from = environment.GameBoard.Board[2, 2];
			ITile targ = environment.GameBoard.Board[2, 4];

			IList<Robot> robots = environment.Robots;
			environment.GameBoard.Board[2, 2].Enter(robots[0]);
			environment.GameBoard.Board[2, 3].Enter(robots[1]);

			var path = environment.PathFinder.FindNotBlockedPath(from, targ);
			Assert.IsNotNull(path);
			Assert.IsNotEmpty(path.Tiles);
			Assert.AreNotEqual(2, path.Tiles.Count);
		}

		[Test]
		public void PathFinding_BlockedPath()
		{
			TestEnvironment environment = new TestEnvironment(5, new Model.MapDB.MapSize(10, 10));
			ITile from = environment.GameBoard.Board[2, 2];
			ITile targ = environment.GameBoard.Board[4, 2];

			IList<Robot> robots = environment.Robots;
			// Blocked robot
			environment.GameBoard.Board[2, 2].Enter(robots[0]);
			// By other robots
			environment.GameBoard.Board[2, 3].Enter(robots[1]);
			environment.GameBoard.Board[3, 2].Enter(robots[2]);
			environment.GameBoard.Board[1, 2].Enter(robots[3]);
			environment.GameBoard.Board[2, 1].Enter(robots[4]);

			var path = environment.PathFinder.FindNotBlockedPath(from, targ);
			Assert.IsNull(path);

			var blockedPath = environment.PathFinder.FindPath(from, targ);
			Assert.IsNotNull(blockedPath);
		}
    }
}
