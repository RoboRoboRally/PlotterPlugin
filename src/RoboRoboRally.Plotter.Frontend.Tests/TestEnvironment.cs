﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Plotter.Frontend.Game;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using RoboRoboRally.Plotter.Frontend.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Tests
{
    class TestEnvironment
    {
		public IList<Robot> Robots { get; private set; }
		public Antenna Antenna { get; private set; }
		public GameBoard GameBoard { get; private set; }
		public ShortestPathFinder PathFinder { get; private set; }
		public CollisionAvoider CollisionAvoider { get; private set; }
		
		public TestEnvironment(int robotsCount, MapSize mapSize)
		{
			GameBoard = new GameBoard(mapSize);
			Robots = new List<Robot>();
			for (int i = 0; i < robotsCount; ++i)
				Robots.Add(new Robot(GameBoard.StorageTiles[i], GameBoard.StorageTiles[i]));
			Antenna = new Antenna(GameBoard.AntennaStorage, GameBoard.AntennaStorage);
			PathFinder = new ShortestPathFinder(GameBoard, Robots, Antenna);
			CollisionAvoider = new CollisionAvoider(GameBoard);
		}
    }
}
