﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Communication.Serial
{
	/// <summary>
	/// Abstract base class for serial communication (derive from this class to provide driver with different implementation)
	/// </summary>
	abstract class SerialCommunicationBase : IDisposable
	{
		#region SERIAL_PORT_SETUP
		/// <summary>
		/// Default baudrate used for serial communication
		/// </summary>
		public const int Baudrate = 115200;
		/// <summary>
		/// By default there is no parity
		/// </summary>
		public const Parity DataParity = Parity.None;
		/// <summary>
		/// Default number of bits to transmit
		/// </summary>
		public int Bits = 8;
		/// <summary>
		/// Default number of bits to indicate end of transmitted data
		/// </summary>
		public StopBits StopBits = StopBits.One;
		/// <summary>
		/// Default line terminator
		/// </summary>
		public const char LineTerminator = '\n';
		#endregion

		/// <summary>
		/// Signalizes that message was received
		/// </summary>
		public event EventHandler<string> MessageReceived;

		/// <summary>
		/// Event handler for message received
		/// </summary>
		/// <param name="message">Received message</param>
		protected virtual void OnMessageReceived(string message)
		{
			// Copy EventHandler to avoid possible race conditions
			EventHandler<string> handler = MessageReceived;
			MessageReceived?.Invoke(this, message);
		}

		/// <summary>
		/// Send message over serial port
		/// </summary>
		/// <param name="message">Message to send</param>
		public abstract void Send(string message);

		/// <summary>
		/// Try to open connection with a device
		/// </summary>
		/// <param name="discoverMessage">Discover message to expect from device</param>
		/// <param name="maxDelay">Maximum delay within which device must respond</param>
		/// <returns>True if successful; False otherwise</returns>
		public abstract bool Connect(string discoverMessage, TimeSpan maxDelay);


		public abstract void Dispose();
	}
}
