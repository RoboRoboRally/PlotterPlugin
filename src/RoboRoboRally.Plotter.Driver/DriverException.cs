﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver
{
	/// <summary>
	/// Exception thrown whenever a hardware error is discovered on plotter
	/// </summary>
    public class DriverException : Exception
    {
		public DriverException(string msg)
			: base(msg)
		{

		}
    }
}
