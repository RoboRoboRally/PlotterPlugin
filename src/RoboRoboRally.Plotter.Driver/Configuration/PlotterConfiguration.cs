﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Configuration
{
	/// <summary>
	/// Plotter hardware configuration
	/// </summary>
    public class PlotterConfiguration
    {

		#region STEPPERS_CONSTANTS
		// Following feedrate constants must match the definition in file 'plotter.robo.h'
		// Notes:
		//		  * Feedrate is speed expressed in mm/minute form
		//		  * Maximum feedrate is total maximum which is allowed for corresponding motor
		//        * Search feedrate is used during initial homing of individual motors
		//        * Maximum dragging feedrate is speed used while moving board elements on game board

		/// <summary>
		/// Maximum feedrate for motors on X, Y axes (both values have to be same - TeaCup firmware requirement)
		/// </summary>
		public const int MaximumFeedrateXY = 16500;
		/// <summary>
		/// Maximum feedrate for motor on Z axis
		/// </summary>
		public const int MaximumFeedrateZ = 2000;
		/// <summary>
		/// Maximum feedrate for motor on E(R) axis, which is responsible for rotating board elements
		/// </summary>
		public const int MaximumFeedrateE = 750;
		/// <summary>
		/// Maximum search feedrate for motors X,Y
		/// </summary>
		public const int SearchFeedrateXY = 8500;
		/// <summary>
		/// Maximum search feedrate for motor Z
		/// </summary>
		public const int SearchFeedrateZ = 800;
		/// <summary>
		/// Maximum search feedrate for motor E(R)
		/// </summary>
		public const int SearchFeedrateE = 500;

		// This constant does not need to match 'plotter.robo.h'
		// - Otherwise should be determined individually for each project
		// - Depends on board elements weight, friction etc...

		/// <summary>
		/// Maximum dragging feedrate for XY motors
		/// </summary>
		public const int MaximumDraggingFeedrateXY = 12000;

		/// <summary>
		/// Value used to attach Z motor
		/// </summary>
		public const int AttachedValueZ = -29;
		/// <summary>
		/// Value used to detach Z motor
		/// </summary>
		public const int DetachedValueZ = +29;
		
		/// <summary>
		/// Constant used to convert between degrees and steps of motor E(R)
		/// </summary>
		public const float DistancePerDegreeE = 0.1f;
		/// <summary>
		/// Steps for motor E(R) to perform 90 degrees rotation
		/// </summary>
		public const int RotationQuarterValue = 9;
		/// <summary>
		/// Steps for motor E(R) to perform 180 degrees rotation
		/// </summary>
		public const int RotationHalfValue = 18;
		/// <summary>
		/// Steps for motor E(R) to perform 360 degrees rotation
		/// </summary>
		public const int RotationWholeValue = 36;
		#endregion

		#region PLOTTER_SIZE_CONSTANTS
		/// <summary>
		/// Default value for <see cref="WorkingAreaWidth"/>
		/// </summary>
		public const int DefaultPlotterWidth = 31500;
		/// <summary>
		/// Default value for <see cref="WorkingAreaHeight"/>
		/// </summary>
		public const int DefaultPlotterHeight = 38500;
		#endregion

		/// <summary>
		/// Plotter working are width
		/// </summary>
		public int WorkingAreaWidth { get; private set; }
		/// <summary>
		/// Plotter working are height
		/// </summary>
		public int WorkingAreaHeight { get; private set; }

		/// <summary>
		/// PlotterConfiguration struct constructor
		/// </summary>
		/// <param name="width">Working area width</param>
		/// <param name="height">Working area height</param>
		public PlotterConfiguration(int width = DefaultPlotterWidth, int height = DefaultPlotterHeight)
		{
			WorkingAreaWidth = width;
			WorkingAreaHeight = height;
		}
    }
}
