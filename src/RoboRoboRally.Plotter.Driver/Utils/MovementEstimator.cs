﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Utils
{
	/// <summary>
	/// Calculates how long do different movements take on physical plotter
	/// </summary>
    static class MovementEstimator
    {
		private static int tolerableOperationDelay = 10000;

		/// <summary>
		/// Calculate timespan for movement on X, Y axes
		/// </summary>
		/// <param name="state">Plotter state</param>
		/// <param name="target">Target vector</param>
		/// <param name="feedrate">Motor feedrate</param>
		/// <returns>Time in milliseconds</returns>
		public static TimeSpan CalculateXYMovementTime(IPlotterState state, Vector2 target, int feedrate)
		{
			int distance = (int)Math.Ceiling(target.GetDistanceFrom(state.Position));
			return TimeSpan.FromMilliseconds(CalculateMovementTime(distance, feedrate));
		}

		/// <summary>
		/// Calculate timespan for movement on Z, R axes
		/// </summary>
		/// <param name="distance">Travel distance</param>
		/// <param name="feedrate">Motor feedrate</param>
		/// <returns></returns>
		public static TimeSpan CalculateZRMovementTime(int distance, int feedrate)
		{
			return TimeSpan.FromMilliseconds(CalculateMovementTime(distance, feedrate));
		}

		private static int CalculateMovementTime(int distance, int feedrate)
		{
			// Note: feedrate represents speed in: mm/min
			// >> we get mm/s as follows: feedrate / 60
			// >> result time in seconds is than calculated as distanceInMillimeters / (feedrate / 60)
			// >> result time in milliseconds is than calculate as distanceInMillimeters / (feedrate / 60) * 1000
			double timeInMilliseconds = Math.Abs(distance) / (feedrate / 60f) * 1000;
			// Additionally we add small constant to eliminate small time imprecisions
			return (int)Math.Ceiling(timeInMilliseconds) + tolerableOperationDelay;
		}
	}
}
