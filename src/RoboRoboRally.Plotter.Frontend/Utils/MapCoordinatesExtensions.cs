﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Utils
{
	/// <summary>
	/// MapCoordinates extension methods
	/// </summary>
	static class MapCoordinatesExtensions
	{
		/// <summary>
		/// Convert to Vector2 (tile coordinates)
		/// </summary>
		/// <param name="coords">This MapCoordinates</param>
		/// <returns>Vector2 - virtual</returns>
		public static Vector2 ToVector2(this MapCoordinates coords)
		{
			return new Vector2(coords.ColIndex, coords.RowIndex);
		}

		/// <summary>
		/// Convert to Vector2 (physical coordinates)
		/// </summary>
		/// <param name="coords">This MapCoordinates</param>
		/// <returns>Vector2 - physical</returns>
		public static Vector2 ToPhysicalCoordinates(this MapCoordinates coords)
		{
			return new Vector2(coords.ColIndex * GameBoard.TileWidth, coords.RowIndex * GameBoard.TileHeight);
		}
    }
}
