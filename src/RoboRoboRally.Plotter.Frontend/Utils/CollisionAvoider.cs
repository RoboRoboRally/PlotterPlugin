﻿using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Frontend.Game;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Utils
{
	/// <summary>
	/// System responsible for collision resolving along paths
	/// </summary>
    class CollisionAvoider
    {
		private GameBoard GameBoard { get; set; }
		private ICollection<Vector2> NeighbouringTiles = new List<Vector2>()
		{
			new Vector2(0, -1),
			new Vector2(-1, 0),
			new Vector2(0, 1),
			new Vector2(1, 0)
		};

		/// <summary>
		/// CollisionAvoider class constructor
		/// </summary>
		/// <param name="board">Game board</param>
		public CollisionAvoider(GameBoard board)
		{
			this.GameBoard = board;
		}

		private IEnumerable<ITile> FindNonBlockedTile(ITile tile, IList<ITile> path, IList<ITile> robotPositions, Antenna antenna)
		{
			Queue<ITile> queue = new Queue<ITile>();
			HashSet<ITile> visited = new HashSet<ITile>();

			void PopulateQueue(ITile current)
			{
				foreach (var coords in NeighbouringTiles)
				{
					var neighbourCoords = current.Position + coords;
					// Neighbour is outside of game board's bounds - skip
					if (!GameBoard.ContainsTileWithPosition(neighbourCoords))
						continue;

					var neighbourTile = GameBoard.Board[neighbourCoords.Y, neighbourCoords.X];
					// Neighbour is not empty - skip
					if (robotPositions.Contains(neighbourTile) || visited.Contains(neighbourTile))
						continue;
					// Antenna can not be moved
					if (antenna.CurrentTile == neighbourTile)
						continue;

					queue.Enqueue(neighbourTile);
					visited.Add(neighbourTile);
				}
			}

			PopulateQueue(tile);
			// Search for closest empty tiles
			while (queue.Count != 0)
			{
				var currentTile = queue.Dequeue();

				// If robot is inactive we want to shift it to a storage tile
				if ((tile.IsStorage && !currentTile.IsStorage) || path.Contains(currentTile))
				{
					PopulateQueue(currentTile);
					continue;
				}

				yield return currentTile;
				PopulateQueue(currentTile);
			}
		}

		/// <summary>
		/// Resolves collisions on provided path
		/// </summary>
		/// <param name="path">Path</param>
		/// <param name="robots">Robots</param>
		/// <param name="antenna">Antenna</param>
		/// <param name="reorganizations">Reorganizations - swaps to perform to clear the path</param>
		public void ResolveCollisionsForPath(Path path, IList<Robot> robots, Antenna antenna, out List<KeyValuePair<Robot, ITile>> reorganizations)
		{
			reorganizations = new List<KeyValuePair<Robot, ITile>>();
			List<ITile> robotPositions = robots.Select(robot => robot.CurrentTile).ToList();

			// Find all tiles that need to be freed
			var blockedTiles = path.Tiles.Where(tile => (tile.Robot != null)).Reverse().ToList();

			// Loop until we resolved all collisions
			while (blockedTiles.Count > 1 || 
				(blockedTiles.Count == 1 && blockedTiles.First() != path.Tiles.First()))
			{
				// Remember tile we freed
				ITile noLongerBlocked = null;
				foreach (var blocked in blockedTiles)
				{
					ITile moveTo = FindNonBlockedTile(blocked, path.Tiles, robotPositions, antenna).FirstOrDefault();
					if (moveTo != null)
					{
						noLongerBlocked = blocked;
						reorganizations.Add(new KeyValuePair<Robot, ITile>(blocked.Robot, moveTo));
						robotPositions.Remove(blocked);
						robotPositions.Add(moveTo);
						break;
					}
				}
				if (noLongerBlocked == null)
					throw new PlotterFrontendException("Could not resolve collisions.");
				blockedTiles.Remove(noLongerBlocked);
				noLongerBlocked = null;
			}
		}
    }
}
