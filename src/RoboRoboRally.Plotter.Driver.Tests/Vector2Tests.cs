﻿using NUnit.Framework;

namespace RoboRoboRally.Plotter.Driver.Tests
{
	[TestFixture]
    public class Vector2Tests
    {
		[Test]
		public void Vector2_Normalize()
		{
			var vect1 = new Vector2(10, 0);
			var vect2 = new Vector2(0, 10);
			var vect3 = new Vector2(-5, 0);
			var vect4 = new Vector2(0, -5);

			Assert.AreEqual(Vector2.UnitX, vect1.Normalize());
			Assert.AreEqual(Vector2.UnitY, vect2.Normalize());
			Assert.AreEqual(Vector2.UnitX * -1, vect3.Normalize());
			Assert.AreEqual(Vector2.UnitY * -1, vect4.Normalize());
		}
    }
}
