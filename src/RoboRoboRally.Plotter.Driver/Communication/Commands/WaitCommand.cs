﻿using System;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Command waits for finish of the previous command (blocks until the previous is finished)
	/// </summary>
	class WaitCommand : BaseCommand
	{
		private const string CommandFormat = "G4 P0";

		/// <summary>
		/// WaitCommand class constructor
		/// </summary>
		/// <param name="maxDuration">Max duration of the command</param>
		public WaitCommand(TimeSpan maxDuration)
			: base(maxDuration)
		{

		}

		public override string ToString()
		{
			return CommandFormat;
		}
	}
}
