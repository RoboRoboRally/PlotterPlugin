﻿using System.Threading;
using System.Threading.Tasks;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Enum of the individual states a command can be in
	/// </summary>
	enum CommandState
	{
		/// <summary>
		/// Command has been sent
		/// </summary>
		Sent,
		/// <summary>
		/// Command has been sent and acknowledged by the firmware
		/// </summary>
		Waiting,
		/// <summary>
		/// Command was acknowledged by the firmware and its execution has terminated
		/// </summary>
		Finished
	}

	/// <summary>
	/// Wrapper for command tracking
	/// </summary>
    class CommandRecord
    {
		/// <summary>
		/// State of the command
		/// </summary>
		public CommandState State { get; internal set; }
		/// <summary>
		/// Command completion source
		/// </summary>
		public TaskCompletionSource<Response> CommandTask { get; private set; }
		/// <summary>
		/// Actual command we are tracking
		/// </summary>
		public BaseCommand Command { get; private set; }
		/// <summary>
		/// Command cancellation source
		/// </summary>
		public CancellationTokenRegistration CancellationRegistration { get; private set; }

		/// <summary>
		/// CommandRecord class constructor
		/// </summary>
		/// <param name="command">Command to track</param>
		/// <param name="task">TaskCompletionSource for the command</param>
		/// <param name="registration">Source for command cancellation</param>
		public CommandRecord(BaseCommand command, TaskCompletionSource<Response> task, CancellationTokenRegistration registration)
		{
			Command = command;
			CommandTask = task;
			CancellationRegistration = registration;
		}
    }
}
