﻿using NUnit.Framework;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Plotter.Frontend.Utils;

namespace RoboRoboRally.Plotter.Frontend.Tests
{
	[TestFixture]
    public class DirectionExtensionTests
    {
		[Test]
		public void DirectionExtension_ToAngle()
		{
			Assert.AreEqual(90, new Direction(DirectionType.Up).ToAngle());
			Assert.AreEqual(-90, new Direction(DirectionType.Down).ToAngle());
			Assert.AreEqual(180, new Direction(DirectionType.Left).ToAngle());
			Assert.AreEqual(0, new Direction(DirectionType.Right).ToAngle());
		}
    }
}
