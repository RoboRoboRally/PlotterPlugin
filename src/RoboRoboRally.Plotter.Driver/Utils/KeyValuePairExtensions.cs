﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Utils
{
	/// <summary>
	/// KeyValuePair exntesion methods
	/// </summary>
    public static class KeyValuePairExtensions
    {
		/// <summary>
		/// Deconstruct for KeyValuePair
		/// </summary>
		/// <typeparam name="TKey">Key type</typeparam>
		/// <typeparam name="TValue">Value type</typeparam>
		/// <param name="record">Instance</param>
		/// <param name="key">Key</param>
		/// <param name="value">Value</param>
		public static void Deconstruct<TKey, TValue>(this KeyValuePair<TKey, TValue> record, out TKey key, out TValue value)
		{
			key = record.Key;
			value = record.Value;
		}
    }
}
