# PlotterDriverControl

This project serves as a testing program for plotter and RoboRoboRally.Plotter.Driver

### Setup game board
* Tiles __1-6__ are storage tiles for robots
* Tile __A__ is storage tile for antenna (not yet implemented)
* Tiles __'#'__ represent pit
* Tiles __'s'__ and __'.'__ represent regular game tiles (first are from starting plan, others from the main game plan)
* Tile __'H'__ is home position for plotter

```
|A|1|2|3|4|5|6|#|#|#|#|#|   
|#|s|s|s|s|s|s|s|s|s|s|#|   
|#|s|s|s|s|s|s|s|s|s|s|#|    
|#|s|s|s|s|s|s|s|s|s|s|#|   
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|#|.|.|.|.|.|.|.|.|.|.|#|    
|H|#|#|#|#|#|#|#|#|#|#|#|

    (microcontroller)
```

* __Initial direction__ for robots is following: __↓__ (robot faces microcontroller)
* __When homed, plotter's head should be right in the middle of the home tile__
* Make sure the __gameplan is aligned__ nicely for presentation
  * Aligning on one side is not necessarily enough (board holding legs are not precise)
  * This may result in plotter dragging board elements imprecisely

### Testing
* Connect to Arduino using USB-B cable
* Plug-in power cable to CNC shield (not to Arduino board!)
* Start PlotterDriverControl
  * This should automatically home both X and Y axes
  * R axis should be manually homed after this; home direction is following __↓__ (facing microcontroller) 
  * even though we did not explicitly home Z axis it should be homed "automatically" after this test
* ```move x250 y250``` moves plotter from the home position to the bottom-left regular game tile
* Place a robot on this tile in the following direction: __→__ (looking the the right)
* ```dance1``` tests movement and counter-clockwise rotations; at the end robot should be on its initial position
* ```dance2``` tests movement and clockwise rotations; at the end robot should be on its initial position
* If everything worked plotter is ready to use!

### Supported commands
* ```help```: shows information for supported commands (all commands should be case-insensitive)
* ```move x<val> y<val>```: moves plotter by relative distance (driver checks that this movement is valid - does not break plotter)
  * One unit is equal to 0.01cm 
  * Example: ```move x250 y250``` moves plotter by 2.5cm in X and Y axes (exactly one game tile)
* ```rotate <val> c|cc```: rotates plotter head by given angle in degrees (0-360), c - clockwise-rotation, cc - counterclockwise-rotation
  * Example: ```rotate 360 cc``` rotates plotter's head by 360 degrees in counterclockwise direction
  * Note: for easier debugging this command calls first ```attach``` and after rotation ```detach``` so it can be used to actually rotate robots
* ```attach```: attaches plotter head (usable only when head is detached - default position)
* ```detach```: detaches plotter head (usable only when head is attached)
* ```home```: homes X and Y axes
* ```dance1```: movement + counter-clockwise rotation test
* ```dance2```: movement + clockwise rotation test

### Overheating

* Make sure to give the device rest from time to time (especially when playing AI vs AI)
* What can get hot:
  * motor Z (responsible for attaching/detaching plotter head)
  * motor R (rotating)
  * motor drivers for X and Y axes

### Troubleshooting

Some of the problems which occurred during development and might happen again in future. (work-in-progress list)

* __Unable to communicate with microcontroller (does not greet user with "start\n\ok\n" on serial port)__
  * verify that device is correctly recognized by operating system
     * (_Windows_) My Computer -> Manage -> Device Manager (it should be COMx device). If its unrecognized it probably means that driver for Arduino is not installed
     * (_Linux_) ```lsusb``` should show this device as TTYx
  * did you try turning it off and on again? (seriously it already happened once during development)

* __Motor spins in the opposite direction than commanded__
  * switch polarity of one motor coil - motor driver headers on CNC shield (either ABCD to BACD or ABCD to ABDC)

* __When commanded to go in a certain direction motor goes randomly back and forth__
  * one of the motor cables is probably disconnected from CNC shield