﻿using System;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Command for reading data from limit switches
	/// </summary>
    class LimitSwitchStatusCommand : BaseCommand
    {
		private const string CommandFormat = "M119";

		/// <summary>
		/// LimitSwitchStatusCommand class constructor
		/// </summary>
		/// <param name="maxDuration">Max duration</param>
		public LimitSwitchStatusCommand(TimeSpan maxDuration)
			: base(maxDuration)
		{

		}

		public override string ToString()
		{
			return CommandFormat;
		}
	}
}
