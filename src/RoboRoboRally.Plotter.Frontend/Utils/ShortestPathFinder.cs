﻿using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System;
using System.Collections.Generic;
using System.Text;
using RoyT.AStar;
using RoboRoboRally.Plotter.Frontend.Game;

namespace RoboRoboRally.Plotter.Frontend.Utils
{
	/// <summary>
	/// System responsible for finding shortest paths. Built with RoyT.Astar library
	/// </summary>
    class ShortestPathFinder
    {
		private const int DefaultCellTravelCost = 1;
		private const int RobotOnBoardMoveCost = 100;
		private const int RobotOnStorageMoveCost = 10;
		private GameBoard GameBoard { get; set; }
		private ICollection<Robot> Robots { get; set; }
		private Antenna Antenna { get; set; }

		/// <summary>
		/// ShortestPathFinder class constructor
		/// </summary>
		/// <param name="gameboard">Game board</param>
		/// <param name="robots">All robots in game</param>
		/// <param name="antenna">Antenna</param>
		public ShortestPathFinder(GameBoard gameboard, ICollection<Robot> robots, Antenna antenna)
		{
			this.GameBoard = gameboard;
			this.Robots = robots;
			this.Antenna = antenna;
		}

		/// <summary>
		/// Tries to find a minimal cost path. No robot shifting allowed
		/// </summary>
		/// <param name="from">Starting tile</param>
		/// <param name="to">End tile</param>
		/// <returns>Path if exists</returns>
		public Path FindNotBlockedPath(ITile from, ITile to)
		{
			return FindCustomPath(from, to, new Action<Grid, Robot>((map, robot) =>
			{
				map.BlockCell(robot.CurrentTile.Position.ToPosition());
			}));
		}

		/// <summary>
		/// Tries to find a minimal cost path. Can also shift other robots
		/// </summary>
		/// <param name="from">Starting tile</param>
		/// <param name="to">End tile</param>
		/// <returns>Path if exists</returns>
		public Path FindPath(ITile from, ITile to)
		{
			return FindCustomPath(from, to, new Action<Grid, Robot>((map, robot) => 
			{
				var cost = (robot.CurrentTile.IsStorage) ? RobotOnStorageMoveCost : RobotOnBoardMoveCost;
				map.SetCellCost(robot.CurrentTile.Position.ToPosition(), cost);
			}));
		}

		private Path FindCustomPath(ITile from, ITile to, Action<Grid, Robot> robotCostFunction)
		{
			var map = new Grid(GameBoard.Width, GameBoard.Height, DefaultCellTravelCost);
			// Set higher cost for cells with robots
			foreach (var robot in Robots)
				robotCostFunction.Invoke(map, robot);
			// Antenna cannot be moved
			map.BlockCell(Antenna.CurrentTile.Position.ToPosition());

			var path = map.GetPath(from.Position.ToPosition(), to.Position.ToPosition(), MovementPatterns.LateralOnly);
			
			// Convert result to match required output format
			if (path.Length == 0)
				return null;

			var output = new List<ITile>(path.Length);
			foreach (var point in path)
				output.Add(GameBoard.Board[point.Y, point.X]);

			return new Path(output);
		}
    }
}
