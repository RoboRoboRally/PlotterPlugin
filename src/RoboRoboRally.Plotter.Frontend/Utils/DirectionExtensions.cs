﻿using RoboRoboRally.Model.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Utils
{
	/// <summary>
	/// Direction extension methods
	/// </summary>
    public static class DirectionExtensions
    {
		/// <summary>
		/// Get angle difference in degrees
		/// </summary>
		/// <param name="current">This direction</param>
		/// <param name="other">Other direction</param>
		/// <returns>Angle difference</returns>
		public static int GetAngleDifferenceWith(this Direction current, Direction other)
		{
			int currentAngle = current.ToAngle();
			int otherAngle = other.ToAngle();
			int differenceAngle = Math.Abs(currentAngle - otherAngle) % 360;

			differenceAngle = (currentAngle < otherAngle) ?
				-differenceAngle : differenceAngle;

			return differenceAngle;
		}

		/// <summary>
		/// Gets angle difference with other angle
		/// </summary>
		/// <param name="current">This direction</param>
		/// <param name="other">Angle</param>
		/// <returns>Angle difference</returns>
		public static int GetAngleDifferenceWith(this Direction current, int other)
		{
			int currentAngle = current.ToAngle();
			int differenceAngle = Math.Abs(currentAngle - other);

			differenceAngle = (currentAngle < other) ?
				-differenceAngle : differenceAngle;

			return differenceAngle;
		}

		/// <summary>
		/// Convert direction to angle
		/// </summary>
		/// <param name="direction">This direction</param>
		/// <returns>Angle</returns>
		public static int ToAngle(this Direction direction)
		{
			return (int)(Math.Atan2(direction.Y, direction.X) * (180.0 / Math.PI)) % 360;
		}
    }
}
