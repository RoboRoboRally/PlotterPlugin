﻿using NUnit.Framework;
using System;

using RoboRoboRally.Plotter.Driver.Communication.Commands;
using RoboRoboRally.Plotter.Driver.Configuration;

namespace RoboRoboRally.Plotter.Driver.Tests
{
	[TestFixture]
    class CommandsTests
    {
		[Test]
		public void Command_AttachGenerating()
		{
			Assert.AreEqual($"G1 Z{PlotterConfiguration.AttachedValueZ} F{PlotterConfiguration.MaximumFeedrateZ}", 
				new MoveZCommand(PlotterConfiguration.AttachedValueZ, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_DetachGenerating()
		{
			Assert.AreEqual($"G1 Z{PlotterConfiguration.DetachedValueZ} F{PlotterConfiguration.MaximumFeedrateZ}", 
				new MoveZCommand(PlotterConfiguration.DetachedValueZ, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_HomeXGenerating()
		{
			Assert.AreEqual($"G28 X", new HomeAxisCommand(AxisType.X, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_HomeYGenerating()
		{
			Assert.AreEqual($"G28 Y", new HomeAxisCommand(AxisType.Y, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_HomeZGenerating()
		{
			Assert.AreEqual($"G28 Z", new HomeAxisCommand(AxisType.Z, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_HomeEGenerating()
		{
			Assert.AreEqual($"G28 E", new HomeAxisCommand(AxisType.E, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_LimitSwitchStatusGenerating()
		{
			Assert.AreEqual($"M119", new LimitSwitchStatusCommand(TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_MoveGenerating()
		{
			Assert.AreEqual($"G1 X100 Y100 F{PlotterConfiguration.MaximumDraggingFeedrateXY}",
				new MoveXYCommand(new Vector2(100, 100), PlotterConfiguration.MaximumDraggingFeedrateXY, TimeSpan.FromMilliseconds(0)).ToString());
			Assert.AreEqual($"G1 X0 Y100 F{PlotterConfiguration.MaximumDraggingFeedrateXY}", 
				new MoveXYCommand(new Vector2(0, 100), PlotterConfiguration.MaximumDraggingFeedrateXY, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_RelativeModeGenerating()
		{
			Assert.AreEqual($"G91", new RelativeModeCommand(TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_RotateGenerating()
		{
			Assert.AreEqual($"G1 E{PlotterConfiguration.RotationQuarterValue} F{PlotterConfiguration.MaximumFeedrateE}", 
				new MoveRCommand(PlotterConfiguration.RotationQuarterValue, PlotterConfiguration.MaximumFeedrateE, TimeSpan.FromMilliseconds(0)).ToString());
			Assert.AreEqual($"G1 E-{PlotterConfiguration.RotationQuarterValue} F{PlotterConfiguration.MaximumFeedrateE}",	
				new MoveRCommand(-PlotterConfiguration.RotationQuarterValue, PlotterConfiguration.MaximumFeedrateE, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_SetPositionGenerating()
		{
			Assert.AreEqual("G92 X1 Y2 Z3 E4"
				, new SetPositionCommand(0, 1, 2, 3, TimeSpan.FromMilliseconds(0)).ToString());
		}

		[Test]
		public void Command_WaitGenerating()
		{
			Assert.AreEqual($"G4 P0", new WaitCommand(TimeSpan.FromMilliseconds(0)).ToString());
		}
    }
}
