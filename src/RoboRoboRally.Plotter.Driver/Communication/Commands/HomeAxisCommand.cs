﻿using RoboRoboRally.Plotter.Driver.Utils;
using System;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// All axes of our plotter
	/// </summary>
	enum AxisType
	{
		/// <summary>
		/// X axis
		/// </summary>
		X,
		/// <summary>
		/// Y axis
		/// </summary>
		Y,
		/// <summary>
		/// Z axis (attaching to game board)
		/// </summary>
		Z,
		/// <summary>
		/// E(R) axis (rotating board elements)
		/// </summary>
		E
	}

	/// <summary>
	/// Command for homing individual axes
	/// </summary>
    class HomeAxisCommand : BaseCommand
    {
		/// <summary>
		/// Axis to home
		/// </summary>
		public AxisType Type { get; private set; }
		private const string Format = "G28 {0}";

		/// <summary>
		/// HomeAxisCommand class constructor
		/// </summary>
		/// <param name="type">Axis type to home</param>
		/// <param name="maxDuration">Max duration for command</param>
		public HomeAxisCommand(AxisType type, TimeSpan maxDuration)
			: base(maxDuration)
		{
			this.Type = type;
		}

		public override string ToString()
		{
			return String.Format(Format, Type.ToDescriptionString());
		}
	}
}
