﻿using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Frontend.Game;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Game.Board
{
	/// <summary>
	/// Common interface for game tiles
	/// </summary>
    public interface ITile
    {
		/// <summary>
		/// Robot slot
		/// </summary>
		Robot Robot { get; }
		/// <summary>
		/// Position of the tile
		/// </summary>
		Vector2 Position { get; }
		/// <summary>
		/// Determines whether this tile is marked as storage
		/// </summary>
		bool IsStorage { get; }

		/// <summary>
		/// Robot enter
		/// </summary>
		/// <param name="robot">Robot</param>
		void Enter(Robot robot);
		/// <summary>
		/// Robot leave
		/// </summary>
		/// <returns>Previously stored robot</returns>
		Robot Leave();
    }
}
