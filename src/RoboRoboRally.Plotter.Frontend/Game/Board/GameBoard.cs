﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Plotter.Driver;

using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Game.Board
{
	/// <summary>
	/// Grid game board which consists of tiles <see cref="GameTile"/>
	/// </summary>
    class GameBoard
    {
		/// <summary>
		/// Game board represented as 2-dimensional array
		/// </summary>
		public ITile[,] Board { get; private set; }
		/// <summary>
		/// Game board width
		/// </summary>
		public int Width { get; private set; }
		/// <summary>
		/// Game board height
		/// </summary>
		public int Height { get; private set; }

		/// <summary>
		/// Width of a game tile
		/// </summary>
		public const int TileWidth = 270;
		/// <summary>
		/// Height of a game tile
		/// </summary>
		public const int TileHeight = 270;
		/// <summary>
		/// Robot count
		/// </summary>
		public const int RobotsCount = 6;

		/// <summary>
		/// Offset from storage tiles
		/// </summary>
		public static Vector2 GameTilesOffset = new Vector2(1, 1);

		/// <summary>
		/// Collection of all storage tiles
		/// </summary>
		public List<ITile> StorageTiles { get; set; }
		/// <summary>
		/// Storage for antenna
		/// </summary>
		public ITile AntennaStorage { get; set; }

		/// <summary>
		/// GameBoard class constructor
		/// </summary>
		/// <param name="mapSize">Size of map</param>
		public GameBoard(MapSize mapSize)
		{
			/* Game board has following layout:
			 * - where character '.' (dot) represents outer pit
			 * - (this also serves as default storage points for robots)
			 * - actual game-board would be located in the middle
			 * 
			 * +---+---+---+---+
			 * | . | . | . | . |
			 * +---+---+---+---+
			 * | . | . | . | . |
			 * +---+---+---+---+
			 * | . |       | . |
			 * +---+       +---+
			 * | . |       | . |
			 * +---+---+---+---+
			 * | . | . | . | . |
			 * +---+---+---+---+
			 */

			// We add one tile on left and right
			int actualWidth = mapSize.ColCount + 2;
			// We add three tiles (one on the top and two in the bottom)
			int actualHeight = mapSize.RowCount + 2;

			Board = new ITile[actualHeight, actualWidth];
			Width = actualWidth;
			Height = actualHeight;

			for (int i = 0; i < Height; ++i)
			{
				for (int j = 0; j < Width; ++j)
				{
					if (i == 0 || i == Height - 1 || j == 0 || j == Width - 1)
						Board[i, j] = new GameTile(new Vector2(j, i), true);
					else
						Board[i, j] = new GameTile(new Vector2(j, i));
				}
			}

			StorageTiles = new List<ITile>(Width);
			for (int i = Width - 2; i > 1; --i)
				StorageTiles.Add(Board[Height - 1, i]);
			AntennaStorage = Board[Height - 1, 0];
		}

		/// <summary>
		/// Checks whether a tile with given position is within game board bounds
		/// </summary>
		/// <param name="position">Position</param>
		/// <returns>True if such tile is within game board bounds; False otherwise</returns>
		public bool ContainsTileWithPosition(Vector2 position)
		{
			if (position.X < 0 || position.Y < 0 ||
				position.X >= Width || position.Y >= Height)
				return false;
			return true;
		}
    }
}
