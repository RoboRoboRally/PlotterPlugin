﻿using Common.Logging;

using RoboRoboRally.Plotter.Driver.Communication;
using RoboRoboRally.Plotter.Driver.Communication.Commands;
using RoboRoboRally.Plotter.Driver.Configuration;
using RoboRoboRally.Plotter.Driver.Utils;

using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

// We will be testing also internal classes and methods
[assembly: InternalsVisibleTo("RoboRoboRally.Plotter.Driver.Tests")]

namespace RoboRoboRally.Plotter.Driver
{
	/// <summary>
	/// Implementation of <see cref="IPlotterController"/> required for plotter controlling
	/// </summary>
	public class PlotterController : IPlotterController, IDisposable
	{
		/// <summary>
		/// Current plotter state
		/// </summary>
		public IPlotterState State { get; protected set; }
		/// <summary>
		/// Current configuration of game board
		/// </summary>
		public BoardConfiguration BoardConfiguration { get; protected set; }
		/// <summary>
		/// Current configuration of plotter
		/// </summary>
		public PlotterConfiguration PlotterConfiguration { get; protected set; }

		/// <summary>
		/// Serial commmunication with plotter
		/// </summary>
		internal TeaCupCommunicator Communication { get; private set; }
		/// <summary>
		/// Mutable device state
		/// </summary>
		internal PlotterState InternalDeviceState { get; private set; }
		private ILog Logger { get; } = LogManager.GetLogger<PlotterController>();	
		private int maxDistancePerCommand = 1500;
		
		/// <summary>
		/// PlotterController class constructor
		/// </summary>
		/// <param name="plotter">Plotter configuration</param>
		/// <param name="board">Board configuration</param>
		public PlotterController(PlotterConfiguration plotter, BoardConfiguration board)
		{
			// Check that plotter's working area is valid
			if ((plotter.WorkingAreaWidth <= 0) || (plotter.WorkingAreaHeight <= 0))
				throw new ArgumentException($"Arguments {nameof(plotter.WorkingAreaWidth)}, {nameof(plotter.WorkingAreaHeight)} need to be greater than zero.");
			// Check that starting point (bottom-left corner) is not negative
			if ((board.X < 0) || (board.Y < 0))
				throw new ArgumentException($"Arguments { nameof(board.X)}, { nameof(board.Y)} need to be greater or equal to zero.");
			// Check that user defined area is actually within plotter's bounds
			if ((board.X + board.Width >= plotter.WorkingAreaWidth) || (board.Y + board.Height >= plotter.WorkingAreaHeight))
				throw new ArgumentException($"Defined area is out of plotter's bounds.");

			PlotterConfiguration = plotter;
			BoardConfiguration = board;
			InternalDeviceState = new PlotterState { Connected = false };
			State = InternalDeviceState;
		}

		/// <inheritdoc />
		public bool Connect()
		{
			try
			{
				// Setup and test serial serial communication
				Communication = new TeaCupCommunicator(new Action(InternalDeviceState.SetFault));
				Communication.SendCommands(new PingCommand(TeaCupCommunicator.MaxNonMovementOperationDelay)).Wait();
			}
			catch (DriverException)
			{
				return false;
			}
			InternalDeviceState.Connected = true;
			return true;
		}

		/// <inheritdoc />
		public bool Initialize()
		{
			// Check that we are connected
			if (!State.Connected)
			{
				// If not try to connect first
				if (!Connect())
					return false;
			}

			InitPlotter();
			return true;
		}

		private void InitPlotter()
		{
			Communication.SendCommands(
				// We are going to use relative commands
				new RelativeModeCommand(TeaCupCommunicator.MaxDiscoverDelay)).Wait();

			// Initial homing
#if SERIAL_MOCK
			/* We don't need to home with mock implementation */
#else
			HomeAxes();
#endif

			var shiftVector = new Vector2(BoardConfiguration.X, BoardConfiguration.Y);
			var shiftDuration = MovementEstimator.CalculateXYMovementTime(InternalDeviceState, shiftVector, PlotterConfiguration.MaximumFeedrateXY);

			Communication.SendCommands(
				// Shift plotter by defined offset
				new MoveXYCommand(shiftVector, PlotterConfiguration.MaximumFeedrateXY, shiftDuration),
				// Mark this position as home
				new SetPositionCommand(shiftVector.X, shiftVector.Y, 0, 0, TeaCupCommunicator.MaxNonMovementOperationDelay)).Wait();

			// TODO: here can be added other initialization commands
		}

		private void CheckDevice()
		{
			if (State.Fault)
				throw new DriverException("Unrecoverable device fault occurred - please reconnect the driver");
			if (!State.Connected)
				throw new DriverException("Connection to device was not established yet.");
		}

		/// <inheritdoc />
		public void Move(Vector2 transition)
		{
			CheckDevice();

			Vector2 currentPosition = State.Position;
			Vector2 targetPosition = currentPosition + transition;
			Logger.Info($"Movement: current({currentPosition}), target({targetPosition})");
			if (!BoardConfiguration.Contains(targetPosition))
				throw new ArgumentException($"Could not perform {nameof(MoveXYCommand)} - target destination was out of board bounds");

			// Determine highest possible speed for current plotter's state
			int feedrate = (State.HeadAttached)
				? PlotterConfiguration.MaximumDraggingFeedrateXY : PlotterConfiguration.MaximumFeedrateXY;

			var movementCommands = MovementSplitter(transition, targetPosition, feedrate);

			Communication.SendCommands(movementCommands).Wait();
			InternalDeviceState.ChangePositionBy(transition);
		}

		/// <inheritdoc />
		public void Rotate(int angle)
		{
			CheckDevice();
			angle = (int)(angle * PlotterConfiguration.DistancePerDegreeE);
			int feedrate = PlotterConfiguration.MaximumFeedrateE;
			Logger.Info($"Rotation: current({State.Rotation}), target({State.Rotation + angle})");

			var command = new MoveRCommand(
				angle,
				feedrate,
				MovementEstimator.CalculateZRMovementTime(angle, feedrate));

			Communication.SendCommands(command).Wait();
			InternalDeviceState.ChangeRotation(angle);
		}

		/// <inheritdoc />
		public void Attach()
		{
			CheckDevice();
			if (State.HeadAttached)
				throw new InvalidOperationException($"Could not perform {nameof(MoveZCommand)}. Plotter's head already attached.");

			var command = new MoveZCommand(
				PlotterConfiguration.AttachedValueZ,
				MovementEstimator.CalculateZRMovementTime(PlotterConfiguration.DetachedValueZ, PlotterConfiguration.MaximumFeedrateZ));

			Communication.SendCommands(command).Wait();
			InternalDeviceState.HeadAttached = true;
		}

		/// <inheritdoc />
		public void Detach()
		{
			CheckDevice();
			if (!State.HeadAttached)
				throw new InvalidOperationException($"Could not perform {nameof(MoveZCommand)}. Plotter's head already detached");

			var command = new MoveZCommand(
				PlotterConfiguration.DetachedValueZ,
				MovementEstimator.CalculateZRMovementTime(PlotterConfiguration.DetachedValueZ, PlotterConfiguration.MaximumFeedrateZ));

			Communication.SendCommands(command).Wait();
			InternalDeviceState.HeadAttached = false;
		}

		/// <inheritdoc />
		public void HomeAxes()
		{
			CheckDevice();
			var commandX = new HomeAxisCommand(
				AxisType.X,	MovementEstimator.CalculateXYMovementTime(State, new Vector2(BoardConfiguration.Width, 0), 
				PlotterConfiguration.SearchFeedrateXY));
			var commandY = new HomeAxisCommand(
				AxisType.Y, MovementEstimator.CalculateXYMovementTime(State, new Vector2(0, BoardConfiguration.Height), 
				PlotterConfiguration.SearchFeedrateXY));
			var commandZ = new HomeAxisCommand(
				AxisType.Z, MovementEstimator.CalculateZRMovementTime(PlotterConfiguration.DetachedValueZ, 
				PlotterConfiguration.SearchFeedrateZ));
			var commandR = new HomeAxisCommand(
				AxisType.E, MovementEstimator.CalculateZRMovementTime(360, 
				PlotterConfiguration.SearchFeedrateE));

			// Homing X-axis
			// - we need to check the switch being triggered in a cycle
			// - for some reason it can end even before it being physically triggered (when travelling longer distances)
			while (!GetLimitSwitchStatus().triggeredX)
				Communication.SendCommands(commandX).Wait();

			// Homing Y-axis
			// - we need to check the switch being triggered in a cycle
			// - for some reason it can end even before it being physically triggered (when travelling longer distances)
			while (!GetLimitSwitchStatus().triggeredY)
				Communication.SendCommands(commandY).Wait();

			// Homing Z-axis
			// - no longer need to test in a loop
			Communication.SendCommands(commandZ).Wait();

			// Homing R-axis
			// - no longer need to test in a loop
			// - this is a small hardware hack - whenever we home axis R it is signalized on Z switch
			// - this is due to the fact that TeaCup firmware does not support homing of the fourth axis
			// - we use slightly modified TeaCup which however supports this, however there are limitations:
			// (1.) we cannot test switch being pressed in a loop (it just does not work, don't know why though...)
			// (2.) we need to home in attached state (so that Z axis did not press the "same" switch)
			// (3.) after homing we need to move head a little bit
			var rotationValue = (int)(40 * PlotterConfiguration.DistancePerDegreeE);
			var compensateForHome = (int)(10 * PlotterConfiguration.DistancePerDegreeE);

			var rotationDuration = MovementEstimator.CalculateZRMovementTime(rotationValue, PlotterConfiguration.MaximumFeedrateE);
			var rotateFront = new MoveRCommand(rotationValue, PlotterConfiguration.MaximumFeedrateE, rotationDuration);
			var rotateBack = new MoveRCommand(-rotationValue + compensateForHome, PlotterConfiguration.MaximumFeedrateE, rotationDuration);

			Attach();
			Communication.SendCommands(
				commandR,
				rotateFront,
				commandZ,
				rotateBack).Wait();

			// Reset initial positions
			InternalDeviceState.Position = new Vector2(0, 0);
			InternalDeviceState.ChangeRotation(0);
			InternalDeviceState.HeadAttached = false;
		}

		/// <inheritdoc />
		public (bool triggeredX, bool triggeredY, bool triggeredZ) GetLimitSwitchStatus()
		{
			CheckDevice();
			Task<Response> response = Communication.SendCommands(new LimitSwitchStatusCommand(TeaCupCommunicator.MaxNonMovementOperationDelay));
			string[] tokens = response.Result.Data.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			int prefixLength = "*_min:".Length;
			bool triggeredX = (tokens[0].Substring(prefixLength) == "triggered") ? true : false;
			bool triggeredY = (tokens[1].Substring(prefixLength) == "triggered") ? true : false;
			bool triggeredZ = (tokens[2].Substring(prefixLength) == "triggered") ? true : false;

			return (triggeredX, triggeredY, triggeredZ);
		}

		private BaseCommand[] MovementSplitter(Vector2 transition, Vector2 targetPosition, int feedrate)
		{
			// Make sure the movement is not too long
			BaseCommand[] movementCommands = null;
			var moveDistance = transition.GetLength();
			var quotient = maxDistancePerCommand / moveDistance;
			var leftOver = new Vector2(transition.X, transition.Y);
			movementCommands = new BaseCommand[(int)Math.Ceiling(moveDistance / maxDistancePerCommand)];
			for (int i = 0; i < movementCommands.Length - 1; ++i)
			{
				var lerp = Vector2.Lerp(State.Position, targetPosition, (float)quotient);
				movementCommands[i] =
					new MoveXYCommand(lerp - State.Position, feedrate, MovementEstimator.CalculateXYMovementTime(State, lerp, feedrate));
				leftOver -= ((MoveXYCommand)movementCommands[i]).Transition;
			}
			movementCommands[movementCommands.Length - 1] =
				new MoveXYCommand(leftOver, feedrate, MovementEstimator.CalculateXYMovementTime(State, leftOver, feedrate));

			return movementCommands;
		}

		#region IDisposable
		private bool isDisposed = false;
		public void Dispose()
		{
			if (!isDisposed)
			{
				isDisposed = true;
				Communication.Dispose();
			}
		}
		#endregion
	}
}
