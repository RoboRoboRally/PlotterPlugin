# PlotterPlugin (RoboRoboRally.Plotter.Driver, RoboRoboRally.Plotter.Frontend)

The aim of this project is to develop a driver and frontend-client for our modification of MakeBlock PlotterXY v2.0.    
The plotter was modified so that it can move and rotate physical elements on game board.

Created driver (RoboRoboRally.Plotter.Driver) can be easily used to instruct the plotter to perform the mentioned operations.   
Additionally using the frontend-client (RoboRoboRally.Plotter.Frontend), the plotter can be added to a RoboRoboRally game.

# Language, dependencies and building

PlotterPlugin is written entirely in C#. We are targeting .NET Standard version 2.0 and this plugin has following external dependencues:
* **RoboRoboRally.Model**: game logic (required by frontend)
* **RoboRoboRally.Server.Interfaces**: communication with server (required by frontend)
* **System.IO.Ports**: serial communication with microcontroller (required by driver)
* **RoyT.AStar**: path-finding for collision resolving (required by frontend)

When building RoboRoboRally.Plotter.Driver there is also possibility to define symbol **SERIAL_MOCK**. This symbol can be useful    
for debugging or when developing without access to the plotter. It won't crash on connection failure. Additionally it will trick     
driver into thinking that all movement commands were immediately finished.


# Repurposing driver for different projects

If you are thinking of repurposing the modified mechanical plotter to different projects this little guide is certainly for you!

### Architecture

Certainly the most important class in the whole project is **PlotterController**, which implements **IPlotterController** interface.

```csharp
namespace RoboRoboRally.Plotter.Driver
{
    public interface IPlotterController : IDisposable
    {
		IPlotterState State { get; }
		BoardConfiguration BoardConfiguration { get; }
		PlotterConfiguration PlotterConfiguration { get; }

		bool Connect()
		void Move(Vector2 difference);
		void Rotate(int differenceAngle);
		void Attach();
		void Detach();
		void HomeAxes();
		(bool triggeredX, bool triggeredY, bool triggeredZ) GetLimitSwitchStatus()
	}
}
```

Important characteristics of this solution:
* **Methods are NOT thread-safe!**
  * However, internal implementation is asynchronous and therefore performance should not be an issue.

* **Units used by driver**
  * Whenever writing distance it should be expressed in millimeters x 10
  * Whenever writing rotation difference it should be expressed in angles
      * Counter-clockwise rotations are negative, clockwise positive

* **Driver performs checks and so the plotter should not break on invalid command**
  * Whenever instructed to go out of bounds **ArgumentException** will be thrown
  * Whenever instructed to attach when the head is already attached **InvalidOperationException** will be thrown
      * (analogically for detached head and calling Detach method)
  * Checks are also performed in constructor when passing **PlotterConfiguration** and **BoardConfiguration**

* **Driver can end up in a fault state**
  * This may happen for example when connection is lost or when TeaCup firmware announces hardware issue
  * Whenever the driver enters fault state it must be restarted (any method call will throw **DriverException**)

# Overheating issues

After some time certain parts of plotter may become really hot. This mostly affects motor Z, responsible for attaching   
and detaching plotter's head to/from game board. However also Polulu DRV8825 drivers for motors X, Y can get hot after   
certain time.

Even though both mentioned parts are engineered to withstand really high temperatures, operator should always keep an eye   
on the plotter to make sure that something does not break due to overheating.