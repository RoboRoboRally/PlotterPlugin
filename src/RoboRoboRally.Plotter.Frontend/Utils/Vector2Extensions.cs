﻿using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Driver.Configuration;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using RoyT.AStar;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Utils
{
	/// <summary>
	/// Vector2 extension methods
	/// </summary>
    static class Vector2Extensions
    {
		/// <summary>
		/// Convert from virtual to physical coordinates
		/// </summary>
		/// <param name="vect">This Vector2</param>
		/// <param name="boardConfiguration">Board configuration</param>
		/// <returns>Physical coordinates</returns>
		public static Vector2 ToPhysicalCoordinates(this Vector2 vect, BoardConfiguration boardConfiguration)
		{
			return new Vector2(
				boardConfiguration.Width - vect.X * GameBoard.TileWidth, 
				vect.Y * GameBoard.TileHeight);
		}

		/// <summary>
		/// Convert from Vector2 to Position
		/// </summary>
		/// <param name="vect">This Vector2</param>
		/// <returns>Position</returns>
		public static Position ToPosition(this Vector2 vect)
		{
			return new Position(vect.X, vect.Y);
		}
    }
}
