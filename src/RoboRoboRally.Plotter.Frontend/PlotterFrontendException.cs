﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend
{
	/// <summary>
	/// Exception thrown whenever frontend discovers an unrecoverable error
	/// </summary>
    class PlotterFrontendException : Exception
    {
		public PlotterFrontendException(string msg) :
			base(msg)
		{  }
    }
}
