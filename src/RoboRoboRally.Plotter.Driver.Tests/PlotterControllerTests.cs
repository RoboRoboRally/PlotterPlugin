﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using RoboRoboRally.Plotter.Driver.Configuration;

namespace RoboRoboRally.Plotter.Driver.Tests
{
	[TestFixture]
	class PlotterControllerTests
	{
		private PlotterController Controller { get; set; }
		private PlotterConfiguration PlotterConfiguration { get; } = new PlotterConfiguration(1000, 2000);
		private BoardConfiguration BoardConfiguration { get; } = new BoardConfiguration(0, 0, 700, 700);

		[OneTimeSetUp]
		public void SetupSerialCommunication()
		{
			Assert.DoesNotThrow(() =>
			{
				Controller = new PlotterController(PlotterConfiguration, BoardConfiguration);
			});
		}

		[OneTimeTearDown]
		public void CloseSerialConnection()
		{
			if (Controller != null)
			{
				Controller.Dispose();
				Controller = null;
			}
		}

		[Test, Order(1)]
		public void Controller_InitTest()
		{
			Assert.IsTrue(Controller.State.Connected == true);
			Assert.IsTrue(Controller.State.Position == new Vector2(BoardConfiguration.X, BoardConfiguration.Y));
			Assert.IsTrue(Controller.State.Rotation == 0);
			Assert.IsTrue(Controller.State.HeadAttached == false);
			Assert.IsTrue(Controller.State.Fault == false);
		}

		[Test, Order(2)]
		public void Controller_MovementTest()
		{
			Controller.Move(new Vector2(100, 200));
			Assert.IsTrue(Controller.State.Position == new Vector2(100, 200));
			Controller.Move(new Vector2(-100, -200));
			Assert.IsTrue(Controller.State.Position == default(Vector2));
		}

		[Test, Order(3)]
		public void Controller_InvalidMovementTest()
		{
			Assert.Throws<ArgumentException>(() => { Controller.Move(new Vector2(-10, 0)); });
			Assert.Throws<ArgumentException>(() => { Controller.Move(new Vector2(0, -10)); });
			Assert.Throws<ArgumentException>(() => { Controller.Move(new Vector2(-10, -10)); });
			Assert.Throws<ArgumentException>(() => { Controller.Move(new Vector2(620, 710)); });
			Assert.Throws<ArgumentException>(() => { Controller.Move(new Vector2(710, 620)); });
			Assert.Throws<ArgumentException>(() => { Controller.Move(new Vector2(710, 710)); });
		}

		[Test, Order(4)]
		public void Controller_AttachTest()
		{
			Controller.Attach();
			Assert.IsTrue(Controller.State.HeadAttached);
		}

		[Test, Order(5)]
		public void Controller_DettachTest()
		{
			Controller.Detach();
			Assert.IsFalse(Controller.State.HeadAttached);
		}

		[Test, Order(6)]
		public void Controller_InvalidDetachTest()
		{
			Assert.Throws<InvalidOperationException>(() => { Controller.Detach(); });
		}

		[Test, Order(7)]
		public void Controller_InvalidAttachTest()
		{
			Controller.Attach();
			Assert.Throws<InvalidOperationException>(() => { Controller.Attach(); });
		}

		[Test, Order(int.MaxValue)]
		public void Controller_InvalidPlotterInit()
		{
			CloseSerialConnection();
			Assert.Throws<ArgumentException>(() => 
			{
				Controller = new PlotterController(
					new PlotterConfiguration(0, 0), 
					new BoardConfiguration(0, 0, 100, 100));
			});
			Assert.Throws<ArgumentException>(() =>
			{
				Controller = new PlotterController(
					new PlotterConfiguration(400, -10), 
					new BoardConfiguration(0, 0, 100, 100));
			});
			Assert.Throws<ArgumentException>(() =>
			{
				Controller = new PlotterController(
					new PlotterConfiguration(400, 400), 
					new BoardConfiguration(-1, 0, 100, 100));
			});
			Assert.Throws<ArgumentException>(() =>
			{
				Controller = new PlotterController(
					new PlotterConfiguration(20, 400),
					new BoardConfiguration(0, 0, 100, 100));
			});
			Assert.Throws<ArgumentException>(() =>
			{
				Controller = new PlotterController(
					new PlotterConfiguration(20, 20),
					new BoardConfiguration(90, 90, 100, 100));
			});
		}
	}
}
