﻿using RoboRoboRally.Plotter.Driver.Configuration;

namespace RoboRoboRally.Plotter.Driver
{
	/// <summary>
	/// Mutable implementation of <see cref="IPlotterState"/> which is used internally
	/// </summary>
	class PlotterState : IPlotterState
	{
		/// <summary>
		/// See <see cref="IPlotterState.Connected"/>
		/// </summary>
		public bool Connected { get; internal set; } = false;
		/// <summary>
		/// See <see cref="IPlotterState.HeadAttached"/>
		/// </summary>
		public bool HeadAttached { get; internal set; } = false;
		/// <summary>
		/// See <see cref="IPlotterState.Position"/>
		/// </summary>
		public Vector2 Position { get; internal set; } = default(Vector2);
		/// <summary>
		/// See <see cref="IPlotterState.Rotation"/>
		/// </summary>
		public int Rotation { get; private set; } = 0;
		/// <summary>
		/// See <see cref="IPlotterState.Fault"/>
		/// </summary>
		public bool Fault { get; private set; } = false;

		/// <summary>
		/// Change position by difference vector
		/// </summary>
		/// <param name="difference">Difference vector</param>
		public void ChangePositionBy(Vector2 difference)
		{
			Position += difference;
		}

		/// <summary>
		/// Change rotation by difference angle
		/// </summary>
		/// <param name="differenceRotation">Difference angle</param>
		public void ChangeRotation(int differenceRotation)
		{
			Rotation += (int)(differenceRotation / PlotterConfiguration.DistancePerDegreeE);
			Rotation %= 360;
		}

		/// <summary>
		/// Fault setter
		/// </summary>
		public void SetFault()
		{
			Fault = true;
		}
    }
}
