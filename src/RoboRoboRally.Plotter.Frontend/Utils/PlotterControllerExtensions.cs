﻿using RoboRoboRally.Model.Util;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Frontend.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RoboRoboRally.Plotter.Frontend.Utils
{ 
	/// <summary>
	/// PlotterController extension methods
	/// </summary>
    public static class PlotterControllerExtensions
    {
		/// <summary>
		/// This constant determines by how much the magnetic head needs to be rotated
		/// before dragging board elements sideways. (value 55 seems to work best)
		/// </summary>
		private const int SidewayMovementRotationValue = 55;

		private static void GotoBoardElementPosition(this IPlotterController plotter, BoardElementBase element)
		{
			// Move to the robot's position
			Vector2 robotPosition = element.CurrentTile.Position;
			Vector2 robotPhysicalPosition = robotPosition.ToPhysicalCoordinates(plotter.BoardConfiguration);
			if (plotter.State.Position != robotPhysicalPosition)
			{
				Vector2 difference = robotPhysicalPosition - plotter.State.Position;
				plotter.Move(difference);
			}		
		}

		private static void GotoBoardElementRotation(this IPlotterController plotter, BoardElementBase element)
		{
			// Rotate head to match the initial rotation of the robot
			if (plotter.State.Rotation != element.Direction.ToAngle())
			{
				int difference = element.Direction.GetAngleDifferenceWith(plotter.State.Rotation);
				plotter.Rotate(difference);
			}
		}

		private static int PrepareHeadForDragging(this IPlotterController plotter, BoardElementBase element, Vector2 difference)
		{
			Vector2 normalizedDifference = difference.Normalize();
			Direction draggingDirection = new Direction(normalizedDifference.X, normalizedDifference.Y);
			
			// If board element is about to be dragged in *supported* direction, we do not have to adjust anything
			if (draggingDirection == element.Direction || draggingDirection == element.Direction.GetOppositeDirection())
				return 0;

			// Otherwise adjust plotter's head
			var angleDifference = element.Direction.GetAngleDifferenceWith(draggingDirection);
			var resultDifference = Math.Sign(angleDifference) * SidewayMovementRotationValue;
			plotter.Rotate(resultDifference);
			return resultDifference;
		}
		
		/// <summary>
		/// Rotates board element (Attach, Rotate, Detach)
		/// </summary>
		/// <param name="plotter">Plotter device</param>
		/// <param name="element">Board element</param>
		/// <param name="differenceAngle">Difference angle</param>
		/// <returns>Real duration of the movement</returns>
		public static TimeSpan RotateBoardElement(this IPlotterController plotter, BoardElementBase element, int differenceAngle)
		{
			DateTime started = DateTime.Now;
			// Make sure we have the same position as robot
			GotoBoardElementPosition(plotter, element);
			GotoBoardElementRotation(plotter, element);

			// Attach head to "pick" robot
			plotter.Attach();
			// Rotate robot
			plotter.Rotate(differenceAngle);
			// Detach head to "release" robot
			plotter.Detach();

			return DateTime.Now - started;
		}

		/// <summary>
		/// Drags a board element (Attach, Move, Detach) to a new position
		/// </summary>
		/// <param name="plotter">Plotter device</param>
		/// <param name="element">Board element</param>
		/// <param name="differenceVector">Difference vector</param>
		/// <param name="differenceAngle">Difference angle</param>
		/// <returns>Real duration of the movement</returns>
		public static TimeSpan DragBoardElement(this IPlotterController plotter, BoardElementBase element, Vector2 differenceVector, int differenceAngle = 0)
		{
			DateTime started = DateTime.Now;
			// Make sure we have the same position as robot
			GotoBoardElementPosition(plotter, element);
			GotoBoardElementRotation(plotter, element);
			// Make sure that we componsate rotation for sideway movements
			var compensateValue = PrepareHeadForDragging(plotter, element, differenceVector);

			// Attach head to "pick" robot
			plotter.Attach();
			// Move robot
			plotter.Move(differenceVector);
			// Make sure that we return robot back to normal rotation
			plotter.Rotate(-1 * compensateValue);
			// Rotate robot
			plotter.Rotate(differenceAngle);
			// Detach head to "release" robot
			plotter.Detach();

			return DateTime.Now - started;
		}

		/// <summary>
		/// Drags board element (Attach, Move, Detach) along a collection of positions
		/// </summary>
		/// <param name="plotter">Plotter device</param>
		/// <param name="element">Board element to drag</param>
		/// <param name="tiles">Collection of positions forming a path</param>
		/// <param name="differenceAngle">Difference angle</param>
		/// <returns>Real duration of the movement</returns>
		public static TimeSpan DragBoardElement(this IPlotterController plotter, BoardElementBase element, IEnumerable<Vector2> tiles, int differenceAngle = 0)
		{
			void MoveToNextWaypoint(Vector2 difference)
			{
				// Make sure we still have correct rotation
				GotoBoardElementRotation(plotter, element);
				// Make sure we compensate rotation for sideway movements
				var compensateValue = PrepareHeadForDragging(plotter, element, difference);
				plotter.Move(difference);
				// Make sure we set the rotation back to normal again
				plotter.Rotate(-1 * compensateValue);
			}

			DateTime started = DateTime.Now;
			// Make sure we have the same position as robot
			GotoBoardElementPosition(plotter, element);
			GotoBoardElementRotation(plotter, element);

			// Attach head to "pick" robot
			plotter.Attach();
			Vector2 currentWaypoint = default(Vector2);
			foreach (var point in tiles)
			{			
				// Merge movements which can be easily resolved using one call
				if (plotter.State.Position.X == point.X || plotter.State.Position.Y == point.Y)
				{
					currentWaypoint = point;
					continue;
				}
				MoveToNextWaypoint(currentWaypoint - plotter.State.Position);
				currentWaypoint = point;
			}
			// Check if we reached end
			if (plotter.State.Position != currentWaypoint)
				MoveToNextWaypoint(currentWaypoint - plotter.State.Position);
			plotter.Rotate(differenceAngle);
			// Detach head to "release" robot
			plotter.Detach();

			return DateTime.Now - started;
		}
    }
}
