﻿using System;

namespace RoboRoboRally.Plotter.Driver
{
	/// <summary>
	/// Two-dimensional vector with integer components
	/// </summary>
    public struct Vector2 : IEquatable<Vector2>
    {
		/// <summary>
		/// X vector component
		/// </summary>
		public int X { get; set; }
		/// <summary>
		/// Y vector component
		/// </summary>
		public int Y { get; set; }
		/// <summary>
		/// Unit vector (1,0)
		/// </summary>
		public static Vector2 UnitX { get; set; } = new Vector2(1, 0);
		/// <summary>
		/// Unit vector (0,1)
		/// </summary>
		public static Vector2 UnitY { get; set; } = new Vector2(0, 1);

		/// <summary>
		/// Vector2 struct constructor
		/// </summary>
		/// <param name="x">X vector component</param>
		/// <param name="y">Y vector component</param>
		public Vector2(int x, int y)
		{
			this.X = x;
			this.Y = y;
		}

		#region OVERLOADS_IEQUATABLE
		public override bool Equals(object obj)
		{
			if (obj is Vector2)
				return Equals(this);
			return false;
		}

		public override int GetHashCode()
		{
			return X.GetHashCode() + Y.GetHashCode();
		}

		public bool Equals(Vector2 other)
		{
			return X == other.X && Y == other.Y;
		}
		#endregion

		#region OPERATORS
		public static bool operator ==(Vector2 v1, Vector2 v2)
		{
			return v1.X == v2.X && v1.Y == v2.Y;
		}

		public static bool operator !=(Vector2 v1, Vector2 v2)
		{
			return v1.X != v2.X || v1.Y != v2.Y;
		}

		public static Vector2 operator +(Vector2 v1, Vector2 v2)
		{
			return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
		}

		public static Vector2 operator -(Vector2 v1, Vector2 v2)
		{
			return new Vector2(v1.X - v2.X, v1.Y - v2.Y);
		}

		public static Vector2 operator *(Vector2 v, int num)
		{
			return new Vector2(v.X * num, v.Y * num);
		}

		public static Vector2 operator *(int num, Vector2 v)
		{
			return v * num;
		}
		#endregion

		/// <summary>
		/// Calculates distance from supplied point
		/// </summary>
		/// <param name="vect">Other vector</param>
		/// <returns>Distance</returns>
		public double GetDistanceFrom(Vector2 vect)
		{
			return Math.Sqrt(Math.Pow(vect.X - X, 2) + Math.Pow(vect.Y - Y, 2));
		}

		/// <summary>
		/// Calculates length of the vector
		/// </summary>
		/// <returns>Length</returns>
		public double GetLength()
		{
			return Math.Sqrt(X * X + Y * Y);
		}

		/// <summary>
		/// Normalizes unit vector multiplied by a constant
		/// </summary>
		/// <returns>Normalized vector</returns>
		public Vector2 Normalize()
		{
			if (!((X == 0) ^ (Y == 0)))
				throw new ArgumentException("Only supports basic four directions");

			int x = (X != 0) ? X / Math.Abs(X) : 0;
			int y = (Y != 0) ? Y / Math.Abs(Y) : 0;
			return new Vector2(x, y);
		}

		/// <summary>
		/// Linear interpolation between two vectors
		/// </summary>
		/// <param name="v1">First vector</param>
		/// <param name="v2">Second vector</param>
		/// <param name="lerpValue">Value</param>
		/// <returns>Linear interpolation</returns>
		public static Vector2 Lerp(Vector2 v1, Vector2 v2, float lerpValue)
		{
			float PrivateLerp(int first, int second, float value)
			{
				return first + (second - first) * value;
			}

			int valX = (int)PrivateLerp(v1.X, v2.X, lerpValue);
			int valY = (int)PrivateLerp(v1.Y, v2.Y, lerpValue);

			return new Vector2(valX, valY);
		}

		public override string ToString()
		{
			return $"X={X}, Y={Y}";
		}
	}
}
