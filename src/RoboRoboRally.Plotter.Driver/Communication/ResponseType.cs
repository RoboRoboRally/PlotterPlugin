﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Communication
{
	/// <summary>
	/// Response type
	/// </summary>
    public enum ResponseType
    {
		/// <summary>
		/// Previous command received / completed successfuly
		/// </summary>
		Ok,
		/// <summary>
		/// An unrecoverable hardware fault discovered by firmware
		/// </summary>
		Fault
    }
}
