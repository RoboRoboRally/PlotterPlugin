﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Base class for all commands
	/// </summary>
	abstract class BaseCommand
	{
		/// <summary>
		/// Maximum timespan needed for successful execution of the command
		/// </summary>
		public TimeSpan MaxDuration { get; protected set; }

		/// <summary>
		/// BaseCommand class constructor
		/// </summary>
		/// <param name="maxduration"></param>
		public BaseCommand(TimeSpan maxduration)
		{
			this.MaxDuration = maxduration;
		}
	}
}
