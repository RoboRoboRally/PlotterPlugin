﻿using System;
using System.Collections.Generic;
using System.Text;

using RoboRoboRally.Model.Util;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using RoboRoboRally.Plotter.Frontend.Utils;
using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Plotter.Frontend.Game
{
	/// <summary>
	/// Dynamic board element which can be moved on game board
	/// </summary>
    public class Robot : BoardElementBase
    {
		/// <summary>
		/// Server's representation of the robot
		/// </summary>
		public RobotInfo Info { get; internal set; }
		/// <summary>
		/// Previous tile of the robot
		/// </summary>
		internal ITile PreviousTile { get; set; } = null;

		/// <summary>
		/// Robot class constructor
		/// </summary>
		/// <param name="storageTile">Storage (initial) tile</param>
		/// <param name="currentTile">Current tile</param>
		public Robot(ITile storageTile, ITile currentTile)
			: base(storageTile, currentTile)
		{
			this.Direction = new Direction(DirectionType.Up);
		}

		public override string ToString()
		{
			string name = (!(Info is null)) ? Info.Name : String.Empty;
			return $"{Info.Name}; {CurrentTile.Position}";
		}
	}
}
