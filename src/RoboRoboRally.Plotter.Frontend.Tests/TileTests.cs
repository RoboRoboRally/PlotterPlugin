﻿using NUnit.Framework;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Frontend.Game;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System;

namespace RoboRoboRally.Plotter.Frontend.Tests
{
	[TestFixture]
    class TileTests
    {
		[Test]
		public void Tile_InitializeTest()
		{
			ITile tile = new GameTile(new Vector2(1, 2));
			Assert.AreEqual(new Vector2(1, 2), tile.Position);
			Assert.IsNull(tile.Robot);
		}

		[Test]
		public void Tile_EnterTest()
		{
			ITile tile = new GameTile(new Vector2(0, 0));
			ITile storageTile = new GameTile(new Vector2(1, 0));

			Robot robot = new Robot(storageTile, tile);
			tile.Enter(robot);
			Assert.IsNull(storageTile.Robot);
			Assert.IsNotNull(tile.Robot);
			Assert.IsNotNull(robot.CurrentTile);

			Assert.AreEqual(tile, robot.CurrentTile);
			Assert.AreEqual(robot, tile.Robot);
		}

		[Test]
		public void Tile_LeaveTest()
		{
			ITile tile = new GameTile(new Vector2(0, 0));
			ITile storageTile = new GameTile(new Vector2(1, 0));

			Robot robot = new Robot(storageTile, storageTile);
			storageTile.Enter(robot);
			Assert.AreEqual(robot, storageTile.Leave());
			Assert.IsNull(storageTile.Robot);
		}

		[Test]
		public void Tile_EnterOccupied()
		{
			ITile tile = new GameTile(new Vector2(0, 0));
			ITile storageTile1 = new GameTile(new Vector2(1, 0));
			ITile storageTile2 = new GameTile(new Vector2(2, 0));

			Robot robot1 = new Robot(storageTile1, storageTile1);
			Robot robot2 = new Robot(storageTile2, storageTile2);
			storageTile1.Enter(robot1);
			storageTile2.Enter(robot2);

			// robot1 goes storageTile1 -> tile
			Assert.DoesNotThrow(() => { tile.Enter(storageTile1.Leave()); });
			// robot2 goes storageTile2 -> tile
			Assert.Throws<InvalidOperationException>(() => { tile.Enter(storageTile2.Leave()); });
		}

		[Test]
		public void Tile_LeaveEmpty()
		{
			ITile tile = new GameTile(new Vector2(0, 0));
			Assert.Throws<InvalidOperationException>(() => { tile.Leave(); });
		}
    }
}
