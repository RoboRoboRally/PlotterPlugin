﻿using NUnit.Framework;
using RoboRoboRally.Plotter.Frontend.Game;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Plotter.Frontend.Tests
{
	[TestFixture]
	public class CollisionAvoiderTests
	{
		/// <summary>
		/// Test that path can be cleared using provided swaps. Also other robots need to be able to reach their original locations.
		/// </summary>
		/// <param name="environment">Test environment</param>
		/// <param name="robot">Robot to move</param>
		/// <param name="path">Path along which the robot is moved</param>
		/// <param name="from">First tile of path</param>
		/// <param name="to">Last tile of path</param>
		/// <param name="swaps">Collision avoiding</param>
		private void TestPathWithSwaps(TestEnvironment environment, Robot robot, IList<ITile> path, ITile from, ITile to, IList<KeyValuePair<Robot, ITile>> swaps)
		{
			Assert.IsNotNull(swaps);
			Assert.IsNotEmpty(swaps);

			// Make reorganizations to unblock the path
			foreach (var (robotToMove, tile) in swaps)
			{
				// Make sure the reorganization is doable (not blocked)
				Assert.IsNotEmpty(environment.PathFinder.FindNotBlockedPath(robotToMove.CurrentTile, tile).Tiles);
				robotToMove.PreviousTile = robotToMove.CurrentTile;
				robotToMove.CurrentTile.Leave();
				tile.Enter(robotToMove);
			}

			// Make sure we can now actually move the robot
			var clearedPath = environment.PathFinder.FindNotBlockedPath(from, to);
			Assert.IsNotNull(clearedPath.Tiles);
			Assert.IsNotEmpty(clearedPath.Tiles);

			// Test we can return other robots to their positions
			swaps.Reverse();
			foreach (var (robotToMove, tile) in swaps)
			{
				if (robotToMove == robot || robotToMove.PreviousTile.IsStorage)
					continue;

				clearedPath = environment.PathFinder.FindNotBlockedPath(robotToMove.CurrentTile, robotToMove.PreviousTile);
				Assert.IsNotNull(clearedPath.Tiles);
				Assert.IsNotEmpty(clearedPath.Tiles);
				ITile newTile = robotToMove.PreviousTile;
				robotToMove.PreviousTile = robotToMove.CurrentTile;
				robotToMove.CurrentTile.Leave();
				newTile.Enter(robotToMove);
			}
		}

		/// <summary>
		/// Testing simple collision
		/// </summary>
		[Test]
		public void CollisionAvoidance_BlockedPathMid()
		{
			TestEnvironment environment = new TestEnvironment(5, new Model.MapDB.MapSize(10, 10));
			ITile from = environment.GameBoard.Board[2, 2];
			ITile targ = environment.GameBoard.Board[4, 2];

			IList<Robot> robots = environment.Robots;
			// Blocked robot
			environment.GameBoard.Board[2, 2].Enter(robots[0]);
			// By other robots
			environment.GameBoard.Board[2, 3].Enter(robots[1]);
			environment.GameBoard.Board[3, 2].Enter(robots[2]);
			environment.GameBoard.Board[1, 2].Enter(robots[3]);
			environment.GameBoard.Board[2, 1].Enter(robots[4]);

			// Try find a path
			var path = environment.PathFinder.FindPath(from, targ);
			environment.CollisionAvoider.ResolveCollisionsForPath(
				path, environment.Robots, environment.Antenna, out List<KeyValuePair<Robot, ITile>> reorganizations);

			TestPathWithSwaps(environment, robots[0], path.Tiles, from, targ, reorganizations);
		}

		/// <summary>
		/// Testing collision on game board's edge where also the initial robot needs to be shifted
		/// </summary>
		[Test]
		public void CollisionAvoidance_HardBlock()
		{
			TestEnvironment environment = new TestEnvironment(5, new Model.MapDB.MapSize(10, 10));
			ITile from = environment.GameBoard.Board[1, 1];
			ITile targ = environment.GameBoard.Board[0, 1];

			IList<Robot> robots = environment.Robots;
			// Blocked robot
			environment.GameBoard.Board[1, 1].Enter(robots[0]);
			// Other robots
			environment.GameBoard.Board[0, 0].Enter(robots[1]);
			environment.GameBoard.Board[0, 1].Enter(robots[2]);
			environment.GameBoard.Board[0, 2].Enter(robots[3]);

			// Non blocking path does not exist
			var nonBlockedPath = environment.PathFinder.FindNotBlockedPath(from, targ);
			Assert.IsNull(nonBlockedPath);

			// Try find shortest blocked path and resolve collisions
			var path = environment.PathFinder.FindPath(from, targ);
			environment.CollisionAvoider.ResolveCollisionsForPath(path, robots, environment.Antenna, out List<KeyValuePair<Robot, ITile>> swaps);

			TestPathWithSwaps(environment, robots[0], path.Tiles, from, targ, swaps);
		}
	}
}
