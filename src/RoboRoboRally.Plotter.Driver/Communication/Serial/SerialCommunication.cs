﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RoboRoboRally.Plotter.Driver.Communication.Serial
{
	/// <summary>
	/// Serial communication extends <see cref="SerialCommunicationBase"/>. Implemented using System.IO.Ports
	/// </summary>
	class SerialCommunication : SerialCommunicationBase
	{
		/// <summary>
		/// Serial port representation
		/// </summary>
		protected SerialPort Port = null;
		/// <summary>
		/// Serial port name
		/// </summary>
		protected string PortName = null;

		/// <inheritdoc />
		public override bool Connect(string discoverMessage, TimeSpan maxDelay)
		{
			// Open communication with every available port and try to find plotter
			foreach (var portName in SerialPort.GetPortNames())
			{
				SerialPort port = new SerialPort(portName, Baudrate, DataParity, Bits, StopBits);
				try
				{
					port.Open();
					port.DtrEnable = true;
					port.NewLine = $"{LineTerminator}";
					// We need to give the other side a chance to react
					Thread.Sleep(maxDelay);
					// We are searching for TeaCup discover message
					if (port.ReadExisting() == discoverMessage)
					{
						PortName = portName;
						Port = port;
						break;
					}
					port.Close();
				}
				// Port might be already open / not available / in incorrect state / unresponsive etc...
				catch (Exception e)
					when (e is UnauthorizedAccessException || e is IOException || e is InvalidOperationException)
				{
					Console.WriteLine($"Serial port {port.PortName} could not be accessed.");
					if (port.IsOpen)
						port.Close();
				}
			}

			// Check if lookup was successful
			if (PortName == null)
				return false;

			// Setup event handlers
			Port.ErrorReceived += Port_ErrorReceived;
			Port.DataReceived += Port_DataReceived;

			// Connection successful
			return true;
		}

		/// <inheritdoc />
		public override void Send(string msg)
		{
			if (Port == null)
				throw new DriverException("Connection has not been initialized.");
			if (!Port.IsOpen)
				throw new DriverException("Connection to the device was lost.");

			Port.WriteLine(msg);
		}

		private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			StringBuilder sb = new StringBuilder();
			while (Port.BytesToRead > 0)
			{
				sb.Append(Port.ReadLine());
				sb.Append(LineTerminator);
			}
			OnMessageReceived(sb.ToString());
		}

		private void Port_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
		{
			OnMessageReceived(Port.ReadLine());
		}

		public override void Dispose()
		{
			if (Port.IsOpen)
				Port.Close();
		}
	}
}
