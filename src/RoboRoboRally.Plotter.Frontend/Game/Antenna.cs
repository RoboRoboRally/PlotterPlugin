﻿using RoboRoboRally.Model.Util;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Game
{
	/// <summary>
	/// Static board element which determines the priority of robots called antenna
	/// </summary>
    class Antenna : BoardElementBase
    {
		/// <summary>
		/// Antenna class constructor
		/// </summary>
		/// <param name="storageTile">Storage (initial) tile</param>
		/// <param name="currentTile">Current tile</param>
		public Antenna(ITile storageTile, ITile currentTile)
			: base(storageTile, currentTile)
		{
		}
	}
}
