﻿using System;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Command for testing responsivense of the device
	/// </summary>
    class PingCommand : BaseCommand
    {
		private const string CommandFormat = "ping";

		/// <summary>
		/// PingCommand class constructor
		/// </summary>
		/// <param name="maxDuration">Max duration of the command</param>
		public PingCommand(TimeSpan maxDuration)
			: base(maxDuration)
		{

		}

		public override string ToString()
		{
			return CommandFormat;
		}
	}
}
