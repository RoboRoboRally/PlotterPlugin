﻿using System;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Command for moving plotter's head (X and Y axes)
	/// </summary>
	class MoveXYCommand : BaseCommand
	{
		/// <summary>
		/// Transition to move by
		/// </summary>
		public Vector2 Transition { get; private set; }
		private int Feedrate { get; set; }
		private const string CommandFormat = "G1 X{0} Y{1} F{2}";

		/// <summary>
		/// MoveXYCommand class constructor
		/// </summary>
		/// <param name="transition">Transition to move by</param>
		/// <param name="feedrate">Feedrate for motors X and Y</param>
		/// <param name="maxDuration">Max duration of the movement</param>
		public MoveXYCommand(Vector2 transition, int feedrate, TimeSpan maxDuration)
			: base(maxDuration)
		{
			Transition = transition;
			Feedrate = feedrate;
		}

		public override string ToString()
		{
			return String.Format(CommandFormat, Transition.X, Transition.Y, Feedrate);
		}
	}
}
