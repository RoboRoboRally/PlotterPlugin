﻿using Common.Logging;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RoboRoboRally.Plotter.Driver.Communication.Commands;
using RoboRoboRally.Plotter.Driver.Communication.Serial;
using RoboRoboRally.Plotter.Driver.Configuration;

namespace RoboRoboRally.Plotter.Driver.Communication
{
	/// <summary>
	/// Communication wrapper for TeaCup firmware
	/// </summary>
	class TeaCupCommunicator : IDisposable
	{
		/// <summary>
		/// TeaCup sends "start" on boot following with "ok" both terminated by '\n'
		/// </summary>
		public const string DiscoverMessage = "start\nok\n";
		/// <summary>
		/// These operations are handled and responded to immediately
		/// </summary>
		public static TimeSpan MaxNonMovementOperationDelay = TimeSpan.FromMilliseconds(200);
		/// <summary>
		/// Arduino should take ~2000milliseconds to reboot and setup TeaCup firmware
		/// </summary>
		public static TimeSpan MaxDiscoverDelay = TimeSpan.FromMilliseconds(3000);
		/// <summary>
		/// Serial connection
		/// </summary>
		internal SerialCommunicationBase SerialPort { get; private set; }
		/// <summary>
		/// Logger - used for logging sent/received messages on DEBUG level
		/// </summary>
		internal ILog Logger { get; private set; } = LogManager.GetLogger<TeaCupCommunicator>();

		private CommandRecord CurrentCommand { get; set; }
		private Response CurrentResponse { get; set; }
		private Action FaultSetter { get; set; }

		/// <summary>
		/// TeaCupCommunicator class constructor
		/// </summary>
		/// <param name="faultSetter">Method to call when hardware fault occurres</param>
		public TeaCupCommunicator(Action faultSetter)
		{		
			bool connectSuccess = false;
			FaultSetter = faultSetter;
#if SERIAL_MOCK
			SerialPort = new MockSerialCommunication();
			connectSuccess = SerialPort.Connect(String.Empty, TimeSpan.FromMilliseconds(0));
#else
			SerialPort = new SerialCommunication();
			connectSuccess = SerialPort.Connect(DiscoverMessage, MaxDiscoverDelay);
#endif
			if (!connectSuccess)
			{
				HandleFaultedTask("Could not establish connection with device");
			}

			CurrentResponse = new Response();
			SerialPort.MessageReceived += SerialPort_MessageReceived;
		}

		/// <summary>
		/// Checks whether we have a valid connection
		/// </summary>
		/// <returns></returns>
		public bool IsConnected()
		{
			try
			{
				SendCommands(new PingCommand(MaxNonMovementOperationDelay)).Wait();
			}
			catch (DriverException)
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// Send multiple commands <seealso cref="BaseCommand"/>
		/// </summary>
		/// <param name="commands">Commands to send</param>
		/// <returns>Task</returns>
		public async Task<Response> SendCommands(params BaseCommand[] commands)
		{
			if (commands.Length == 0)
				return new Response() { Type = ResponseType.Ok };
			Task<Response> currentTask = null;
			try
			{
				// Send all commands one by one
				foreach (var command in commands)
				{
					// Each command can run only for a limited time (otherwise we assume the connection was dropped)
					CancellationTokenSource tokenSource = new CancellationTokenSource();
					tokenSource.CancelAfter(command.MaxDuration);
					var cancallationRegistration = tokenSource.Token.Register(() =>
					{
						CurrentCommand.CommandTask.SetCanceled();
					});
					currentTask = SendCommand(command, cancallationRegistration);
					await currentTask.ConfigureAwait(false);
				}
			}
			catch (OperationCanceledException)
			{
				HandleFaultedTask("Operation took too long - device unresponsive");
			}
			return currentTask.Result;
		}

		private async Task<Response> SendCommand(BaseCommand command, CancellationTokenRegistration registration)
		{
			// Register command
			CurrentCommand = new CommandRecord(command, new TaskCompletionSource<Response>(), registration);

			// Send command over serial port to firmware
			SendData(command.ToString());
			CurrentCommand.State = CommandState.Sent;
			await CurrentCommand.CommandTask.Task.ConfigureAwait(false);

			return CurrentCommand.CommandTask.Task.Result;
		}

		private void SendData(string data)
		{
			try
			{
				SerialPort.Send(data);
			}
			catch (IOException)
			{
				HandleFaultedTask("Operation took too long - device unresponsive");
			}
			Logger.Debug($"Driver_Sent: {data}");
		}

		private void SerialPort_MessageReceived(object sender, string message)
		{
			Logger.Debug($"Driver_Recv: {message}");
			/*
			 * Each response from TeaCup firmware is one of the following five types:
			 *   1) // -- this represents a comment (generally these messages can be ignored)
			 *   2) ok -- previous operation was successful
			 *   3) rs -- requires a command to be resended
			 *   4) !! -- hardware fault (unrecoverable state)
			 *   5) data response (for example: limit switch values)
			 */
			
			// Message can actually be multiline in case of debugging commands
			string[] messageLines = message.Split(new char[] { SerialCommunicationBase.LineTerminator }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var line in messageLines)
			{
				if (line.Trim() == String.Empty)
					continue;
				string[] responseSegments = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				switch (responseSegments[0])
				{
					case "//":
						/* We ignore this message - this is only comment */
						break;
					case "ok":
						// Check if command has been already sent
						if (CurrentCommand.State == CommandState.Sent)
						{
							// Additionally we send WaitCommand
							// (this will block until the previous command is finished)
							CurrentCommand.State = CommandState.Waiting;
							SendData(new WaitCommand(CurrentCommand.Command.MaxDuration).ToString());							
						}
						// Command successfully finished
						else if (CurrentCommand.State == CommandState.Waiting)
						{
							FinishCurrentTaskWithResponse(ResponseType.Ok);
							CurrentResponse = new Response();
						}
						break;
					case "!!":
						// There is nothing we can do about this message
						FinishCurrentTaskWithResponse(ResponseType.Fault);
						HandleFaultedTask("Unrecoverable hardware fault discovered by firmware");
						break;
					default:
						// We received additional response data
						CurrentResponse.Data += line;
						break;
				}
			}
		}

		private void FinishCurrentTaskWithResponse(ResponseType type)
		{
			CurrentResponse.Type = type;
			CurrentCommand.State = CommandState.Finished;
			// Unregister cancellation callback
			CurrentCommand.CancellationRegistration.Dispose();
			// Set task result
			if (!CurrentCommand.CommandTask.TrySetResult(CurrentResponse))
				HandleFaultedTask("Operation took too long - device unresponsive");
		}

		private void HandleFaultedTask(string message)
		{
			FaultSetter();
			Logger.Error(message);
			throw new DriverException(message);
		}

		#region IDisposable
		private bool isDisposed = false;
		public void Dispose()
		{
			if (!isDisposed)
			{
				isDisposed = true;
				SerialPort.Dispose();
			}
		}
		#endregion
	}
}
