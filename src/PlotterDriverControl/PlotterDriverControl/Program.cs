﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Driver.Configuration;

namespace PlotterDriverControl
{
	class Program
	{
		private static IPlotterController Controller { get; set; }
		private const int TileWidth = 270;
		private const int TileHeight = 270;

		private static void Dance1(string[] args)
		{
			Controller.Attach();
			Controller.Move(new Vector2(TileWidth, 0));
			Controller.Rotate(90);
			Controller.Move(new Vector2(0, TileHeight));
			Controller.Rotate(90);
			Controller.Move(new Vector2(-TileWidth, 0));
			Controller.Rotate(90);
			Controller.Move(new Vector2(0, -TileHeight));
			Controller.Rotate(90);
			Controller.Detach();
		}

		private static void Dance2(string[] args)
		{
			Controller.Attach();
			Controller.Rotate(90);
			Controller.Move(new Vector2(0, TileHeight));
			Controller.Rotate(-90);

			Controller.Move(new Vector2(TileWidth, 0));
			Controller.Rotate(-90);
			Controller.Move(new Vector2(0, -TileHeight));
			Controller.Rotate(-90);
			Controller.Move(new Vector2(-TileWidth, 0));
			Controller.Rotate(-90);
			Controller.Move(new Vector2(0, TileHeight));
			Controller.Rotate(-90);

			Controller.Rotate(-90);
			Controller.Move(new Vector2(0, -TileHeight));
			Controller.Rotate(90);
			Controller.Detach();
		}

		private static void ShowHelp(string[] args)
		{
			Console.WriteLine("---------------------------------------------------");
			Console.WriteLine(">> Description: command format");
			Console.WriteLine("---------------------------------------------------");
			Console.WriteLine(">> Moving (1 unit = 0.01cm): move x<val> y<val>");
			Console.WriteLine(">> Moving robot: movedrag x<val> y<val>");
			Console.WriteLine(">> Rotate (value in angles): rotate <val> <c|cc>");
			Console.WriteLine(">> Rotate robot: rotatedrag <val> <c|cc>");
			Console.WriteLine(">> Attach: attach");
			Console.WriteLine(">> Detach: detach");
			Console.WriteLine(">> Home (XY axes supported): home");
			Console.WriteLine(">> Limit switch status: switches");
			Console.WriteLine(">> Test (movement + rotation-c): dance1");
			Console.WriteLine(">> Test (movement + rotation-cc): dance2");
			Console.WriteLine("---------------------------------------------------");
		}

		private static void Move(string[] args)
		{
			int valX = 0;
			int valY = 0;
			for (int i = 1; i < args.Length; ++i)
			{
				try
				{
					if (args[i][0] == 'x')
						valX = int.Parse(args[i].Substring(1));
					if (args[i][0] == 'y')
						valY = int.Parse(args[i].Substring(1));
				}
				catch (FormatException)
				{
					Console.WriteLine(">> Invalid command format. Skipping.");
				}
			}
			Controller.Move(new Vector2(valX, valY));
		}

		private static void MoveDragging(string[] args)
		{
			Controller.Attach();
			Move(args);
			Controller.Detach();
		}

		private static void Rotate(string[] args)
		{
			int value = int.Parse(args[1]);
			if (args[2].Trim() == "c")
				value = -value;

			Controller.Rotate(value);
		}

		private static void RotateDragging(string[] args)
		{
			Controller.Attach();
			Rotate(args);
			Controller.Detach();
		}

		private static void Attach(string[] args)
		{
			Controller.Attach();
		}

		private static void Detach(string[] args)
		{
			Controller.Detach();
		}

		private static void ShowLimitSwitches(string[] args)
		{
			(bool x, bool y, bool z) = Controller.GetLimitSwitchStatus();
			Console.WriteLine($">> StatusX:{x}, StatusY:{y}, StatusZ:{z}");
		}

		private static void Home(string[] args)
		{
			Controller.HomeAxes();
		}


		static void Main(string[] args)
		{
			PlotterConfiguration plotter = new PlotterConfiguration(
				PlotterConfiguration.DefaultPlotterWidth,
				PlotterConfiguration.DefaultPlotterHeight);
			BoardConfiguration board = new BoardConfiguration(
				BoardConfiguration.DefaultOriginX,
				BoardConfiguration.DefaultOriginY,
				BoardConfiguration.DefaultBoardWidth,
				BoardConfiguration.DefaultBoardHeight);
			Controller = new PlotterController(plotter, board);
			Console.WriteLine(">> Attempting to connect and initialize the device...");

			Controller.Connect();
			if (!Controller.Initialize())
			{
				Console.WriteLine(">> Unable to connect to device. Press any key to exit...");
				Console.ReadKey();
				return;
			}
			Console.WriteLine();
			Console.WriteLine($">> For help write \"help\"");

			var commandHandlers = new Dictionary<string, Action<string[]>>()
			{
				{ "move", Move },
				{ "movedrag", MoveDragging },
				{ "rotate", Rotate },
				{ "rotatedrag", RotateDragging },
				{ "attach", Attach },
				{ "detach", Detach },
				{ "home", Home },
				{ "switches", ShowLimitSwitches },
				{ "help", ShowHelp },
				{ "dance1", Dance1 },
				{ "dance2", Dance2 }
			};

			while (true)
			{
				string command = Console.ReadLine().ToLowerInvariant();
				string[] tokens = command.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
				if (tokens.Length == 0)
					continue;

				if (!commandHandlers.ContainsKey(tokens[0]))
				{
					Console.WriteLine(">> Unrecognized command - skipping");
					continue;
				}
				commandHandlers[tokens[0]].Invoke(tokens);
			}
		}
	}
}
