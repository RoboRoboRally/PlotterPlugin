﻿using System;
using System.Collections.Generic;
using System.Text;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Frontend.Game;

namespace RoboRoboRally.Plotter.Frontend.Game.Board
{
	/// <summary>
	/// Representation of a game tile, which implements <see cref="ITile"/>
	/// </summary>
	class GameTile : ITile
	{
		/// <inheritdoc />
		public Robot Robot { get; private set; }
		/// <inheritdoc />
		public Vector2 Position { get; private set; }
		/// <inheritdoc />
		public bool IsStorage { get; private set; }

		/// <summary>
		/// GameTile class constructor
		/// </summary>
		/// <param name="coords">Position of the tile</param>
		/// <param name="storage">Flag which determines whether the tile is storage</param>
		public GameTile(Vector2 coords, bool storage = false)
		{
			Position = coords;
			IsStorage = storage;
		}

		/// <inheritdoc />
		void ITile.Enter(Robot newRobot)
		{
			if (Robot != null)
				throw new InvalidOperationException("An attempt to place two robots on the same tile");

			Robot = newRobot;
			Robot.CurrentTile = this;
		}

		/// <inheritdoc />
		Robot ITile.Leave()
		{
			if (Robot == null)
				throw new InvalidOperationException("An attempt to remove a non-existing robot");

			Robot leavingRobot = Robot;
			Robot = null;
			return leavingRobot;
		}

		public override string ToString()
		{
			return $"[X:{Position.X}, Y:{Position.Y}]";
		}
	}
}
