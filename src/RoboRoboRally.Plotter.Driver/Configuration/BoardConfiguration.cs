﻿using System;
using System.Diagnostics.Contracts;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Configuration
{
	/// <summary>
	/// Game board configuration
	/// </summary>
	public struct BoardConfiguration
	{
		/// <summary>
		/// Default value for <see cref="Width"/>
		/// </summary>
		public const int DefaultBoardWidth = 2970;
		/// <summary>
		/// Default value for <see cref="Height"/>
		/// </summary>
		public const int DefaultBoardHeight = 3780;
		/// <summary>
		/// Default value for <see cref="X"/>
		/// </summary>
		public const int DefaultOriginX = 0;
		/// <summary>
		/// Default value for <see cref="Y"/>
		/// </summary>
		public const int DefaultOriginY = 0;

		/// <summary>
		/// Offset on X axis from plotter's home (in millimeters x10)
		/// </summary>
		public int X { get; private set; }
		/// <summary>
		/// Offset on Y axis from plotter's home (in millimeters x10)
		/// </summary>
		public int Y { get; private set; }
		/// <summary>
		/// Game board width (in millimeters x10)
		/// </summary>
		public int Width { get; private set; }
		/// <summary>
		/// Game board height (in millimeters x10)
		/// </summary>
		public int Height { get; private set; }

		/// <summary>
		/// BoardConfiguration struct constructor
		/// </summary>
		/// <param name="x">X offset</param>
		/// <param name="y">Y offset</param>
		/// <param name="width">Board width</param>
		/// <param name="height">Board height</param>
		public BoardConfiguration(int x = DefaultOriginX, int y = DefaultOriginY, int width = DefaultBoardWidth, int height = DefaultBoardHeight)
		{
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
		}
		
		/// <summary>
		/// Checks whether given point is within board's bounds
		/// </summary>
		/// <param name="point">Point</param>
		/// <returns>True if it is in board's bounds; False otherwise</returns>
		public bool Contains(Vector2 point)
		{
			if (point.X < 0 || point.Y < 0 || point.X > X + Width || point.Y > Y + Height)
				return false;
			return true;
		}
	}
}
