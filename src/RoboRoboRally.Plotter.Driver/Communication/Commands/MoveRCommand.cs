﻿using System;
using RoboRoboRally.Plotter.Driver.Configuration;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Command for rotating plotter's head
	/// </summary>
    class MoveRCommand : BaseCommand
    {
		/// <summary>
		/// Rotation value
		/// </summary>
		public int RotationDifference { get; private set; }
		private int Feedrate { get; set; }
		private const string CommandFormat = "G1 E{0} F{1}";

		/// <summary>
		/// MoveRCommand class constructor
		/// </summary>
		/// <param name="angle">Angle difference to rotate by</param>
		/// <param name="feedrate">Feedrate for motor R</param>
		/// <param name="maxDuration">Max duration for command</param>
		public MoveRCommand(int angle, int feedrate, TimeSpan maxDuration)
			: base(maxDuration)
		{
			Feedrate = feedrate;
			RotationDifference = angle;
		}

		public override string ToString()
		{
			return String.Format(CommandFormat, RotationDifference, Feedrate);
		}
	}
}
