﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Utils
{
	/// <summary>
	/// Enum extension methods
	/// </summary>
    public static class EnumExtensions
    {
		/// <summary>
		/// Get text representation of enum value
		/// </summary>
		/// <param name="instance">Enum</param>
		/// <returns>Text representation</returns>
		public static string ToDescriptionString(this Enum instance)
		{
			return Enum.GetName(instance.GetType(), instance);
		}
    }
}
