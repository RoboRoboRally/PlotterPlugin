﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver
{
	/// <summary>
	/// Holds information about plotter's state
	/// </summary>
	public interface IPlotterState
    {
		/// <summary>
		/// Determines whether the device is connected
		/// </summary>
		bool Connected { get; }
		/// <summary>
		/// Determines whether the magnetic head is attached to the game board
		/// </summary>
		bool HeadAttached { get; }
		/// <summary>
		/// Current physical position of the plotter
		/// </summary>
		Vector2 Position { get; } 
		/// <summary>
		/// Current rotation of the plotter's magnetic head
		/// </summary>
		int Rotation { get; }
		/// <summary>
		/// Determines whether the device is in faulted state
		/// </summary>
		bool Fault { get; }
	}
}
