﻿using System;
using System.Linq;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Driver.Utils;
using RoboRoboRally.Plotter.Frontend.Game;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using RoboRoboRally.Plotter.Frontend.Utils;
using RoboRoboRally.Server.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RoboRoboRally.Plotter.Frontend.Spectators
{
	public partial class GameSpectator : IMovementSpectator
	{
		/// <inheritdoc />
		public void RobotRotated(RobotInfo robot, RotationType rotation)
		{
			WaitForDeviceCooldown();

			// This probably should not happen - however if it does we can ignore it
			if (rotation == RotationType.None)
				return;

			Robot currentRobot = GetRobot(robot);
			// Calculate difference angle for plotter
			Direction currentRotation = currentRobot.Direction;
			Direction targetRotation = currentRotation.Rotate(rotation);
			int differenceAngle = targetRotation.GetAngleDifferenceWith(currentRotation);

			MovementTimeSpan += Plotter.RotateBoardElement(
				currentRobot,
				differenceAngle);

			currentRobot.Direction = targetRotation;
		}

		/// <inheritdoc />
		public void RobotTeleported(RobotInfo robot, MapCoordinates newCoords, Direction direction)
		{
			WaitForDeviceCooldown();
			Robot currentRobot = GetRobot(robot);

			Path CleanPath(ITile from, ITile to, out List<KeyValuePair<Robot, ITile>> reorganizations)
			{
				var pathToClean = PathFinder.FindPath(from, to);
				CollisionAvoider.ResolveCollisionsForPath(pathToClean, Robots, Antenna, out reorganizations);
				foreach (var (robotToMove, tile) in reorganizations)
					RobotMovedNonBlocked(robotToMove, tile);

				return pathToClean;
			}

			void RepopulatePath(List<KeyValuePair<Robot, ITile>> reorganizations)
			{
				foreach (var (robotToMove, _) in reorganizations)
				{
					// We don't need to shift back inactive robots
					// also we don't shift back the robot we actually wanted to move
					if (robotToMove.PreviousTile.IsStorage || robotToMove == currentRobot)
						continue;

					RobotMovedNonBlocked(robotToMove, robotToMove.PreviousTile);
				}
			}

			List<KeyValuePair<Robot, ITile>> swaps = null;
			if (currentRobot.CurrentTile.Position != newCoords.ToVector2())
			{
				// Try to find shortest path
				Vector2 targetTilePosition = newCoords.ToVector2() + GameBoard.GameTilesOffset;
				ITile targetTile = GameBoard.Board[targetTilePosition.Y, targetTilePosition.X];
				var path = PathFinder.FindNotBlockedPath(currentRobot.CurrentTile, targetTile);

				// If no path was found we need to move some of neighbouring robots
				if (path == null)
					path = CleanPath(currentRobot.CurrentTile, targetTile, out swaps);

				// We need to recalculate the path to use physical coordinates before passing to plotter
				var physicalCoords = path.GetPhysicalCoordinates(Plotter.BoardConfiguration);

				// Actually move the robot and rotate robot
				int targetAngle = direction.ToAngle();
				int currentAngle = currentRobot.Direction.ToAngle();
				MovementTimeSpan += Plotter.DragBoardElement(currentRobot, physicalCoords, targetAngle - currentAngle);
				// Change current tile for the moved robot and its direction
				currentRobot.PreviousTile = currentRobot.CurrentTile;
				currentRobot.Direction = direction;
				path.Tiles.Last().Enter(currentRobot.CurrentTile.Leave());
			}
			else if (direction != currentRobot.Direction)
			{
				int targetAngle = direction.ToAngle();
				int currentAngle = currentRobot.Direction.ToAngle();
				MovementTimeSpan += Plotter.RotateBoardElement(currentRobot, targetAngle - currentAngle);
				currentRobot.Direction = direction;
			}

			// If we did any reorganizations, now we should revert them
			if (swaps != null)
				RepopulatePath(swaps);
		}

		/// <inheritdoc />
		public void RobotMoved(RobotInfo robot, MapCoordinates newCoords)
		{
			Robot currentRobot = GetRobot(robot);

			// This is same as teleporting, because model does not resolve all collisions
			RobotTeleported(robot, newCoords, currentRobot.Direction);
		}

		private void RobotMovedNonBlocked(Robot robot, ITile tile)
		{
			var path = PathFinder.FindNotBlockedPath(robot.CurrentTile, tile);
			MovementTimeSpan += Plotter.DragBoardElement(robot, path.GetPhysicalCoordinates(Plotter.BoardConfiguration));
			robot.PreviousTile = robot.CurrentTile;
			path.Tiles.Last().Enter(robot.CurrentTile.Leave());
		}

		/// <inheritdoc />
		public void RobotRemoved(RobotInfo robot)
		{
			Robot currentRobot = GetRobot(robot);
			ITile targetTile = currentRobot.StorageTile;
			MapCoordinates targetTileCoords = new MapCoordinates(
				targetTile.Position.Y - GameBoard.GameTilesOffset.Y,
				targetTile.Position.X - GameBoard.GameTilesOffset.X);

			// This situation is same as teleporting (however we change the target)
			RobotTeleported(robot, targetTileCoords, currentRobot.Direction);
		}

		/// <inheritdoc />
		public void RobotAdded(RobotInfo robot, MapCoordinates coordinates, Direction direction)
		{
			// This situation is handled exactly using the same procedure:
			RobotTeleported(robot, coordinates, direction);
		}

		/// <inheritdoc />
		public void RobotPushed(RobotInfo robot, MapCoordinates coords1, MapCoordinates coords2)
		{
			// Once again this is same situation as robot teleported
			RobotTeleported(robot, coords2, GetRobot(robot).Direction);
		}

		private Robot GetRobot(RobotInfo robotInfo)
		{
			if (!RobotsMapping.ContainsKey(robotInfo))
			{
				RobotsMapping.Add(robotInfo, Robots[robotInfo.Order]);
				RobotsMapping[robotInfo].Info = robotInfo;
			}

			return RobotsMapping[robotInfo];
		}

		private void WaitForDeviceCooldown()
		{
			var totalGameTime = DateTime.Now - GameStartedDateTime;

			if (MovementTimeSpan.TotalSeconds / totalGameTime.TotalSeconds > 0.75)
			{
				var maxAllowedMovement = totalGameTime.TotalSeconds * 0.75;
				var cooldownTime = MovementTimeSpan.TotalSeconds - maxAllowedMovement;
				Task.Delay((int)cooldownTime * 1000).Wait();
			}
		}

		#region Ignored_Methods
		/// <summary>
		/// Ignored
		/// </summary>
		public void RobotRebooting(RobotInfo robot, MapCoordinates newCoords, Direction direction) { /* Purposedly ignored */ }
		#endregion
	}
}