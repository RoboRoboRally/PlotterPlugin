﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Driver.Communication
{
	/// <summary>
	/// Response wrapper
	/// </summary>
    public class Response
    {
		/// <summary>
		/// Additional data (if provided)
		/// </summary>
		public string Data { get; internal set; }
		/// <summary>
		/// Reponse type <seealso cref="ResponseType"/>
		/// </summary>
		public ResponseType Type { get; internal set; }
    }
}
