﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RoboRoboRally.Plotter.Driver.Communication.Serial
{
	/// <summary>
	/// Mock serial communication extends <see cref="SerialCommunicationBase"/>. Useful for debugging.
	/// </summary>
	class MockSerialCommunication : SerialCommunicationBase
	{
		/// <inheritdoc />
		public override bool Connect(string discoverMessage, TimeSpan maxDelay)
		{
			return true;
		}

		/// <inheritdoc />
		public override void Send(string message)
		{
			GenerateResponse(message);
		}

		private void GenerateResponse(string command)
		{
			string commandPrefix = command.Split(new char[] { ' ' }, options: StringSplitOptions.RemoveEmptyEntries).First();
			string response;

			// Only M119 adds additional data as its answer
			if (commandPrefix == "M119")
				response = String.Concat("ok", LineTerminator, "x_min:triggered y_min:triggered z_min:triggered", LineTerminator);
			// Other commands simply respond with ok
			else
				response = String.Concat("ok", LineTerminator);

			// Signalize received message
			OnMessageReceived(response);
		}

		public override void Dispose()
		{

		}
	}
}
