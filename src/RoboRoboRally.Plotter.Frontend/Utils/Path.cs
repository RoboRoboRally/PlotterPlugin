﻿using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Driver.Configuration;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Utils
{
	/// <summary>
	/// Wrapper for collection of tiles which form path
	/// </summary>
    class Path
    {
		/// <summary>
		/// Individual tiles forming the path
		/// </summary>
		public IList<ITile> Tiles { get; private set; }

		/// <summary>
		/// Path class constructor
		/// </summary>
		/// <param name="tiles">Tiles</param>
		public Path(IList<ITile> tiles)
		{
			this.Tiles = tiles;
		}

		/// <summary>
		/// Converts path into physical positions needed by driver
		/// </summary>
		/// <param name="configuration">Board configuration</param>
		/// <returns>Physical path</returns>
		public IList<Vector2> GetPhysicalCoordinates(BoardConfiguration configuration)
		{
			List<Vector2> physicalCoords = new List<Vector2>(Tiles.Count);
			foreach (var point in Tiles)
			{
				Vector2 physicalPosition = point.Position.ToPhysicalCoordinates(configuration);
				physicalCoords.Add(physicalPosition);
			}

			return physicalCoords;
		}
    }
}
