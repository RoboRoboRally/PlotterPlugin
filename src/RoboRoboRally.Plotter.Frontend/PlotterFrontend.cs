﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using RoboRoboRally.Model;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Driver.Configuration;
using RoboRoboRally.Plotter.Frontend.Game;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using RoboRoboRally.Plotter.Frontend.Spectators;
using RoboRoboRally.Server.Interfaces;

// We will be testing also internal classes and methods
[assembly: InternalsVisibleTo("RoboRoboRally.Plotter.Frontend.Tests")]

namespace RoboRoboRally.Plotter.Frontend
{
	/// <summary>
	/// Class representing plotter as a client for RoboRoboRally solution
	/// </summary>
	public class PlotterFrontend : IClient, IPlugin, IDisposable
	{
		/// <summary>
		/// Current lobby we are joined in
		/// </summary>
		public ILobby CurrentGameLobby { get; private set; }
		/// <summary>
		/// Info about this RoboRoboRally plugin <seealso cref="Server.Interfaces.PluginInfo"/>
		/// </summary>
		public PluginInfo PluginInfo { get; private set; }

		/// <summary>
		/// Plotter device <seealso cref="IPlotterController"/>
		/// </summary>
		internal IPlotterController Plotter { get; private set; }
		/// <summary>
		/// Main game spectator 
		/// </summary>
		internal IClient GameSpectator { get; private set; }
		/// <summary>
		/// Collection of all watchers required by server
		/// </summary>
		internal IDictionary<Type, IClient> Watchers { get; private set; }
		/// <summary>
		/// Mapping between server-side robot representation and PlotterFrontend's representation
		/// </summary>
		internal IDictionary<RobotInfo, Robot> RobotsMapping { get; private set; }

		/// <inheritdoc />
		public void Initialize()
		{
			BoardConfiguration BoardConfiguration = new BoardConfiguration(
				BoardConfiguration.DefaultOriginX,
				BoardConfiguration.DefaultOriginY,
				BoardConfiguration.DefaultBoardWidth,
				BoardConfiguration.DefaultBoardHeight);
			PlotterConfiguration PlotterConfiguration = new PlotterConfiguration(
				PlotterConfiguration.DefaultPlotterWidth,
				PlotterConfiguration.DefaultPlotterHeight);
			Plotter = new PlotterController(PlotterConfiguration, BoardConfiguration);

			AssemblyName assemblyInfo = Assembly.GetExecutingAssembly().GetName();
			PluginInfo = new PluginInfo()
			{
				Name = "RoboRobo-Plotter",
				Version = assemblyInfo.Version.ToString(),
				Description = $"Game spectator which is responsible for movement replication of board elements on a physical game board."
			};

			RobotsMapping = new Dictionary<RobotInfo, Robot>();
			GameSpectator = new GameSpectator(Plotter, RobotsMapping);

			Watchers = new Dictionary<Type, IClient>
			{
				{ typeof(IPlugin), this },
				{ typeof(IMovementSpectator), GameSpectator },
				{ typeof(IGameFlowSpectator), GameSpectator }
			};
		}

		/// <inheritdoc />
		public bool CanJoinLobby(ILobbyInfo lobby)
		{
			// Check if we can connect to plotter / or are already connected
			if (!Plotter.State.Connected)
				Plotter.Connect();

			// We can join lobby only if we have plotter and are not already in a lobby
			return Plotter.State.Connected && CurrentGameLobby == null;
		}

		/// <inheritdoc />
		public IDictionary<Type, IClient> JoinLobby(ILobby lobby)
		{
			if (!Plotter.State.Connected)
				throw new InvalidOperationException("Can not join lobby - not connected to device");

			if (!Plotter.Initialize())
				throw new DriverException("Could not initialize device");

			CurrentGameLobby = lobby;
			return Watchers;
		}

		/// <inheritdoc />
		public void LeaveLobby(ILobby lobby)
		{
			CurrentGameLobby = null;
		}

		/// <summary>
		/// Ignored
		/// </summary>
		public void GameStarted(IGame game)	{ /* Ignored on this spectator */ }

		#region IDisposable
		private bool isDisposed = false;
		public void Dispose()
		{
			if (!isDisposed)
			{
				isDisposed = true;
				Plotter.Dispose();
			}
		}
		#endregion
	}
}
