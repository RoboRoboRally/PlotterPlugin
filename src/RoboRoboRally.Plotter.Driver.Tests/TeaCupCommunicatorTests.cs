﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RoboRoboRally.Plotter.Driver.Communication;
using RoboRoboRally.Plotter.Driver.Communication.Commands;
using System.Text.RegularExpressions;
using RoboRoboRally.Plotter.Driver.Configuration;
using RoboRoboRally.Plotter.Driver.Utils;

namespace RoboRoboRally.Plotter.Driver.Tests
{
	[TestFixture]
    class TeaCupCommunicatorTests
	{
		private TeaCupCommunicator Communicator { get; set; }

		[OneTimeSetUp]
		public void SetupSerialCommunication()
		{
			Assert.DoesNotThrow(() => { Communicator = new TeaCupCommunicator(() => { /* empty */ }); });
		}

		[OneTimeTearDown]
		public void SerialCommunicationClose()
		{
			Communicator.Dispose();
			Communicator = null;
		}

		[OneTimeTearDown]
		public void DestroySerialCommunication()
		{
			Communicator.Dispose();
		}

		[Test, Order(1)]
		public void Serial_ConnectionTest()
		{
			Assert.IsTrue(Communicator.IsConnected());			
		}

		[Test, Order(2)]
		public void Serial_CommandTest()
		{
			Assert.DoesNotThrow(() => { Communicator.SendCommands(
				new PingCommand(TeaCupCommunicator.MaxNonMovementOperationDelay)).Wait(); });
		}

		[Test, Order(3)]
		public void Serial_MultipleCommandsTest()
		{
			Assert.DoesNotThrow(() => { Communicator.SendCommands(
				new PingCommand(TeaCupCommunicator.MaxNonMovementOperationDelay)).Wait(); });
			Assert.DoesNotThrow(() => { Communicator.SendCommands(
				new PingCommand(TeaCupCommunicator.MaxNonMovementOperationDelay)).Wait(); });
		}

		[Test, Order(4)]
		public void Serial_BulkCommandsTest()
		{
			Assert.DoesNotThrow(() => { Communicator.SendCommands(
				new PingCommand(TeaCupCommunicator.MaxNonMovementOperationDelay), 
				new LimitSwitchStatusCommand(TeaCupCommunicator.MaxNonMovementOperationDelay)).Wait(); });
		}

		[Test, Order(5)]
		public void Serial_MultilineResponseCommandTest()
		{
			Task<Response> task = Communicator.SendCommands(
				new LimitSwitchStatusCommand(TeaCupCommunicator.MaxNonMovementOperationDelay));

			Assert.AreEqual(ResponseType.Ok, task.Result.Type);
			Assert.IsTrue(Regex.IsMatch(task.Result.Data, @"x_min:(open|triggered) y_min:(open|triggered) z_min:(open|triggered)"));
		}
    }
}
