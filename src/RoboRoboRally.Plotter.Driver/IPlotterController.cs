﻿using RoboRoboRally.Plotter.Driver.Communication.Commands;
using RoboRoboRally.Plotter.Driver.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RoboRoboRally.Plotter.Driver
{
	/// <summary>
	/// Basic interface for controlling plotter
	/// </summary>
    public interface IPlotterController : IDisposable
    {
		/// <summary>
		/// Information about current state of plotter
		/// </summary>
		IPlotterState State { get; }
		/// <summary>
		/// Current board configuration
		/// </summary>
		BoardConfiguration BoardConfiguration { get; }
		/// <summary>
		/// Current plotter configuration
		/// </summary>
		PlotterConfiguration PlotterConfiguration { get; }

		/// <summary>
		/// Tries connecting to plotter
		/// </summary>
		/// <returns>True if successful, False otherwise</returns>
		bool Connect();
		/// <summary>
		/// Initializes plotter
		/// </summary>
		/// <returns>True if initialize was successful, False otherwise</returns>
		bool Initialize();

		/// <summary>
		/// Moves plotter's head to target position
		/// </summary>
		/// <param name="differenceVector">Difference movement vector</param>
		void Move(Vector2 differenceVector);
		/// <summary>
		/// Rotates plotter's head by a certain angle
		/// (if value is negative - clockwise rotation, counterclockwise otherwise)
		/// </summary>
		/// <param name="differenceAngle">Difference rotation in degrees</param>
		void Rotate(int differenceAngle);
		/// <summary>
		/// Attaches magnetic head to game board
		/// </summary>
		void Attach();
		/// <summary>
		/// Detaches magnetic head further from game board
		/// </summary>
		void Detach();
		/// <summary>
		/// All axes are sent to their default position
		/// </summary>
		void HomeAxes();
		/// <summary>
		/// Reads status of limit switches
		/// (Corresponding switches are pressed whenever an axis returns to home position)
		/// </summary>
		/// <returns>Status of limit switches</returns>
		(bool triggeredX, bool triggeredY, bool triggeredZ) GetLimitSwitchStatus();
	}
}
