﻿using System;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Command shifts home-position by defined offset
	/// </summary>
    class SetPositionCommand : BaseCommand
    {
		private int X { get; set; }
		private int Y { get; set; }
		private int Z { get; set; }
		private int R { get; set; }
		private const string CommandFormat = "G92 X{0} Y{1} Z{2} E{3}";

		/// <summary>
		/// SetPositionCommand class constructor
		/// </summary>
		/// <param name="x">X offset</param>
		/// <param name="y">Y offset</param>
		/// <param name="z">Z offset</param>
		/// <param name="r">E(R) offset</param>
		/// <param name="maxDuration">Max duration of the command</param>
		public SetPositionCommand(int x, int y, int z, int r, TimeSpan maxDuration)
			: base(maxDuration)
		{
			this.X = x;
			this.Y = y;
			this.Z = z;
			this.R = r;
		}

		public override string ToString()
		{
			return String.Format(CommandFormat, X, Y, Z, R);
		}
	}
}
