﻿using System;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Command for setting relative mode in TeaCup firmware
	/// </summary>
    class RelativeModeCommand : BaseCommand
    {
		private const string CommandFormat = "G91";
		
		/// <summary>
		/// RelativeModeCommand class constructor
		/// </summary>
		/// <param name="maxDuration">Max duration of the command</param>
		public RelativeModeCommand(TimeSpan maxDuration)
			: base(maxDuration)
		{

		}

		public override string ToString()
		{
			return CommandFormat;
		}
	}
}
