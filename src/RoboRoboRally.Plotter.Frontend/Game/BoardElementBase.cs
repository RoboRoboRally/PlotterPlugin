﻿using RoboRoboRally.Model.Util;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Plotter.Frontend.Game
{
	/// <summary>
	/// Base class for all board elements
	/// </summary>
    public abstract class BoardElementBase
    {
		/// <summary>
		/// Current tile on which the board element can be found
		/// </summary>
		public ITile CurrentTile { get; internal set; }
		/// <summary>
		/// Storage (initial) tile for the board element
		/// </summary>
		public ITile StorageTile { get; internal set; }
		/// <summary>
		/// Direction of the board element
		/// </summary>
		public Direction Direction { get; internal set; }

		public BoardElementBase(ITile storageTile, ITile currentTile)
		{
			this.CurrentTile = currentTile;
			this.StorageTile = storageTile;
		}
	}
}
