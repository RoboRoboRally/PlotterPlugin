﻿using System;

using RoboRoboRally.Plotter.Driver.Configuration;

namespace RoboRoboRally.Plotter.Driver.Communication.Commands
{
	/// <summary>
	/// Command for attaching / detaching plotter's head to / from game board
	/// </summary>
    class MoveZCommand : BaseCommand
    {
		/// <summary>
		/// Movement value for motor Z
		/// </summary>
		public int TransitionZ { get; private set; }
		private const string CommandFormat = "G1 Z{0} F{1}";

		/// <summary>
		/// MoveZCommand class constructor
		/// </summary>
		/// <param name="transition">Transition value for motor Z</param>
		/// <param name="maxDuration">Max duration of the transition</param>
		public MoveZCommand(int transition, TimeSpan maxDuration)
			: base(maxDuration)
		{
			TransitionZ = transition;
		}

		public override string ToString()
		{
			return String.Format(CommandFormat, TransitionZ, PlotterConfiguration.MaximumFeedrateZ);
		}
	}
}
