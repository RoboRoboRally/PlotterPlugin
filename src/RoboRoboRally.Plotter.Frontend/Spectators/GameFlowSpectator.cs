﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RoboRoboRally.Model;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Plotter.Driver;
using RoboRoboRally.Plotter.Driver.Configuration;
using RoboRoboRally.Plotter.Frontend.Game;
using RoboRoboRally.Plotter.Frontend.Game.Board;
using RoboRoboRally.Plotter.Frontend.Utils;
using RoboRoboRally.Server.Interfaces;

namespace RoboRoboRally.Plotter.Frontend.Spectators
{
	/// <summary>
	/// Main game spectator
	/// </summary>
	public partial class GameSpectator : IGameFlowSpectator
	{
		/// <summary>
		/// Plotter device
		/// </summary>
		internal IPlotterController Plotter { get; private set; }
		/// <summary>
		/// Current game board
		/// </summary>
		internal GameBoard GameBoard { get; private set; }
		/// <summary>
		/// Mapping between server's and this plugin's representation for robots
		/// </summary>
		internal IDictionary<RobotInfo, Robot> RobotsMapping { get; private set; }
		/// <summary>
		/// Available robots
		/// </summary>
		internal IList<Robot> Robots { get; private set; }
		/// <summary>
		/// Current antenna
		/// </summary>
		internal Antenna Antenna { get; private set; }
		/// <summary>
		/// Current game
		/// </summary>
		internal IGame Game { get; private set; }

		/// <summary>
		/// Shortest path-finder systen which can be used for path finding on game board
		/// </summary>
		internal ShortestPathFinder PathFinder { get; private set; }
		/// <summary>
		/// Collision avoider system which can be used to resolve collisions on paths
		/// </summary>
		internal CollisionAvoider CollisionAvoider { get; private set; }
		/// <summary>
		/// Timestamp of game start
		/// </summary>
		internal DateTime GameStartedDateTime { get; set; }
		/// <summary>
		/// Timespan for which motors were active
		/// </summary>
		internal TimeSpan MovementTimeSpan { get; set; }

		/// <summary>
		/// GameSpectator class constructor
		/// </summary>
		/// <param name="plotter">Plotter device</param>
		/// <param name="robots">Robots mapping</param>
		internal GameSpectator(IPlotterController plotter, IDictionary<RobotInfo, Robot> robots)
		{
			this.Plotter = plotter;
			this.RobotsMapping = robots;
		}

		/// <inheritdoc />
		public void GameStarted(IGame game)
		{
			this.Game = game;
			GameStartedDateTime = DateTime.Now;
			MovementTimeSpan = default(TimeSpan);

			// Initialize game board and antenna (may change for each game)
			GameBoard = new GameBoard(game.GetMapInfo().MapSize);
			var antennaPosition = Game.GetMapInfo().Settings.PriorityAntenna.Coordinates.ToVector2();
			var antennaTile = GameBoard.Board[antennaPosition.Y + GameBoard.GameTilesOffset.Y, antennaPosition.X + GameBoard.GameTilesOffset.X];
			Antenna = new Antenna(antennaTile, antennaTile);

			// First time initialization of robots
			if (Robots == null)
				InitializeRobots();
			else
			{
				foreach (var robot in Robots)
				{
					var position = robot.CurrentTile.Position;
					robot.CurrentTile.Leave();
					var tile = GameBoard.Board[position.Y, position.X];
					tile.Enter(robot);
				}
			}

			// Initialize path-finding and collision avoidance system
			PathFinder = new ShortestPathFinder(GameBoard, Robots, Antenna);
			CollisionAvoider = new CollisionAvoider(GameBoard);			
		}

		private void InitializeRobots()
		{
			Robots = new List<Robot>();
			for (int i = 0; i < GameBoard.RobotsCount; ++i)
			{
				ITile storageTile = GameBoard.StorageTiles[i];
				Robot newRobot = new Robot(storageTile, storageTile);
				storageTile.Enter(newRobot);
				Robots.Add(newRobot);
			}
		}

		/// <inheritdoc />
		public void GameOver()
		{
			// Return active robots to storage
			foreach (var robot in Robots)
			{
				var storagePosition = robot.StorageTile.Position - GameBoard.GameTilesOffset;
				if (robot.CurrentTile.IsStorage)
					continue;
				RobotTeleported(robot.Info, new MapCoordinates(storagePosition.Y, storagePosition.X), new Direction(DirectionType.Up));
			}
		}

		#region Ignored_Methods
		/// <summary>
		/// Ignored
		/// </summary>
		public void PhaseEnded(string phaseName) { /* Purposedly ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void RobotPriorityDetermined(RobotInfo[] robotsInOrder) { /* Purposedly ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void GameIsEnding() { /* Purposedly ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void RobotFinishedGame(RobotInfo robot) { /* Purposely ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void RobotVisitedCheckpoint(RobotInfo robot, int checkpointNr) { /* Purposely ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void TurnEnded(int turnNumber) { /* Purposely ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void TurnStarted(int turnNumber) { /* Purposely ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void GamePaused() { /* Purposely ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void GameResumed() { /* Purposely ignored */ }
		/// <summary>
		/// Ignored
		/// </summary>
		public void PhaseStarted(string phaseName) { /* Purposely gnored */ }
		#endregion
	}
}

